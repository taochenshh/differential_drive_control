#include "TCS230.h"

TCS230::TCS230()
{
	COLORSENSOR_S0 = 31;
    COLORSENSOR_S1 = 32;
	COLORSENSOR_S2 = 33;
	COLORSENSOR_S3 = 34;
	COLORSENSOR_LED = 35;
	COLORSENSOR_OUT = 36;
}

void TCS230::initcolor(int num)
{
	pinMode(COLORSENSOR_S0,OUTPUT);
	pinMode(COLORSENSOR_S1,OUTPUT);
	pinMode(COLORSENSOR_S2,OUTPUT);
	pinMode(COLORSENSOR_S3,OUTPUT);
	pinMode(COLORSENSOR_LED,OUTPUT);
	pinMode(COLORSENSOR_OUT,INPUT);
 if (num == 1)  // 2%
 {
  digitalWrite(COLORSENSOR_S0,LOW);
   digitalWrite(COLORSENSOR_S1,HIGH);
   scale = 0.02;
 }
 else if (num == 2)  // 20%
 {
  digitalWrite(COLORSENSOR_S0,HIGH);
   digitalWrite(COLORSENSOR_S1,LOW);
   scale = 0.2;
 }
  else if (num == 3)  // 100%
 {
  digitalWrite(COLORSENSOR_S0,HIGH);
   digitalWrite(COLORSENSOR_S1,HIGH);
   scale = 1;
 }
	return;
}

void TCS230::lightOn()
{
	digitalWrite(COLORSENSOR_LED,HIGH);
	return;
}

void TCS230::lightOff()
{
	digitalWrite(COLORSENSOR_LED,LOW);
	return;
}
float TCS230::colorGetFrequency(int filter)
{
	float freq = 0;
	unsigned long Width = 0;
	
	colorSetFilter(filter);
	Width = pulseIn(COLORSENSOR_OUT,HIGH);
	freq = 1000000.0 / Width / 2.0 / scale;
	return(freq);
}

void TCS230::colorSetFilter(int filter)
{
	if (filter == 0)
	{
		digitalWrite(COLORSENSOR_S2, HIGH);
		digitalWrite(COLORSENSOR_S3,  LOW);
	}
	else if (filter == 1)
	{
		digitalWrite(COLORSENSOR_S2, LOW);
		digitalWrite(COLORSENSOR_S3, LOW);
	}
	else if (filter == 2)
	{
		digitalWrite(COLORSENSOR_S2, HIGH);
		digitalWrite(COLORSENSOR_S3, HIGH);
	}
	else if (filter == 3)
	{
		digitalWrite(COLORSENSOR_S2, LOW);
		digitalWrite(COLORSENSOR_S3, HIGH);
	}
	return;
}
void TCS230::colorBlackBalance()
{
    int k = 0;

    blackBalance.red = 0;
    blackBalance.green = 0;
    blackBalance.blue = 0;
    for (k = 0; k < 10; k++)
    { 
      blackBalance.red += colorGetFrequency(RED); 
      delay(10);
    }
  	blackBalance.red /= 10.0;
    Serial.println("Black_red OK");
    for (k = 0; k < 10; k++)
    { 
      blackBalance.green += colorGetFrequency(GREEN); 
      delay(10);
    }
	  blackBalance.green /= 10.0;
    Serial.println("Black_green OK");
    for (k = 0; k < 10; k++)
    { 
      blackBalance.blue += colorGetFrequency(BLUE); 
      delay(10);
    }
    blackBalance.blue /= 10.0;
    Serial.println("Black_blue OK");
    return;
}
void TCS230::colorWhiteBalance()
{
    int k = 0;
    whiteBalance.red = 0;
    whiteBalance.green = 0;
    whiteBalance.blue = 0;
    for (k = 0; k < 10; k++)
    { 
      whiteBalance.red += colorGetFrequency(RED); 
      delay(10);
    }
    whiteBalance.red /= 10.0;
    Serial.println("White_red OK");
    Serial.println(whiteBalance.red);
    for (k = 0; k < 10; k++)
    { 
      whiteBalance.green += colorGetFrequency(GREEN); 
      delay(10);
    }
    whiteBalance.green /= 10.0;
    Serial.println("White_green OK");
        Serial.println(whiteBalance.green);
    for (k = 0; k < 10; k++)
    { 
      whiteBalance.blue += colorGetFrequency(BLUE); 
      delay(10);
    }
    whiteBalance.blue /= 10.0;
    Serial.println("White_blue OK");
        Serial.println(whiteBalance.blue);
    return;
}
void TCS230::setBlackColor(float redfreq, float greenfreq, float bluefreq)
{
	blackBalance.red = redfreq;
	blackBalance.green = greenfreq;
	blackBalance.blue = bluefreq;
	return;
}
void TCS230::setWhiteColor(float redfreq, float greenfreq, float bluefreq)
{
	whiteBalance.red = redfreq;
	whiteBalance.green = greenfreq;
	whiteBalance.blue = bluefreq;
	return;
}
float TCS230::colorConvertFreqToData(float freq, int filter)
{
	float colorData = 0;
	if (filter == 1)
	{
		colorData = 255 * (freq - blackBalance.red) / (whiteBalance.red - blackBalance.red);		
	}
    else if (filter == 2)
	{
	    colorData = 255 * (freq - blackBalance.green) / (whiteBalance.green - blackBalance.green);
	}
	else if (filter ==3)
	{
		colorData = 255 * (freq - blackBalance.blue) / (whiteBalance.blue - blackBalance.blue);
	}
	return(colorData);
}

void TCS230::colorGetColorValue(colorVector<float>* currentColor)
{
	float f_red = 0;
	float f_green = 0;
	float f_blue = 0;
 int k = 0;

  for(k = 0;k < 10; k++)
  {
    f_red += colorGetFrequency(RED);
  }
	f_red /= 10.0;
 Serial.print("f_red:");
 Serial.print(f_red);
 Serial.print("  ****   ");
	(*currentColor).red = colorConvertFreqToData(f_red,RED);
 Serial.println((*currentColor).red);
   for(k = 0;k < 10; k++)
  {
    f_green += colorGetFrequency(GREEN);
  }
  f_green /= 10.0;
  Serial.print("f_green:");
 Serial.print(f_green);
 Serial.print("  ****   ");
	(*currentColor).green = colorConvertFreqToData(f_green,GREEN);
   Serial.println((*currentColor).green);

   for(k = 0;k < 10; k++)
  {
    f_blue += colorGetFrequency(BLUE);
  }
  f_blue /= 10.0;
  Serial.print("f_blue:");
 Serial.print(f_blue);
  Serial.print("  ****   ");
	(*currentColor).blue = colorConvertFreqToData(f_blue,BLUE);
    Serial.println((*currentColor).blue);
    return;	
}

















