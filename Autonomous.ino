
#include <Wire.h>
#include "myVNH5019motor.h"
#include "TCS230.h"
#include "MC33926.h"

/////////////////////////////Compass//////////////////////////////////
/* LSM303 Address definitions */
#define LSM303_MAG  0x1E  // assuming SA0 grounded
#define LSM303_ACC  0x18  // assuming SA0 grounded
 
#define X 0
#define Y 1
#define Z 2
 
/* LSM303 Register definitions */
#define CTRL_REG1_A 0x20
#define CTRL_REG2_A 0x21
#define CTRL_REG3_A 0x22
#define CTRL_REG4_A 0x23
#define CTRL_REG5_A 0x24
#define HP_FILTER_RESET_A 0x25
#define REFERENCE_A 0x26
#define STATUS_REG_A 0x27
#define OUT_X_L_A 0x28
#define OUT_X_H_A 0x29
#define OUT_Y_L_A 0x2A
#define OUT_Y_H_A 0x2B
#define OUT_Z_L_A 0x2C
#define OUT_Z_H_A 0x2D
#define INT1_CFG_A 0x30
#define INT1_SOURCE_A 0x31
#define INT1_THS_A 0x32
#define INT1_DURATION_A 0x33
#define CRA_REG_M 0x00
#define CRB_REG_M 0x01
#define MR_REG_M 0x02
#define OUT_X_H_M 0x03
#define OUT_X_L_M 0x04
#define OUT_Y_H_M 0x05
#define OUT_Y_L_M 0x06
#define OUT_Z_H_M 0x07
#define OUT_Z_L_M 0x08
#define SR_REG_M 0x09
#define IRA_REG_M 0x0A
#define IRB_REG_M 0x0B
#define IRC_REG_M 0x0C

/////////////////////////////////////////////////////////////////////



#define LIMITSWITCH 3
#define APIN 37
#define BPIN 38
#define CPIN 39
#define DPIN 40
#define CH1 41 
#define CH2 42
#define CH3 43
#define CH5 44
#define INTTIME 5
#define TIME 20
#define RIGHTTURN 1
#define LEFTTURN 0

#define SPEEDLIMIT 120.0
#define TURNTIME 1000
#define FORWARDTIME 1000
#define BACKTIME 1000
#define CPUFREQUENCY 16000000
#define PRESCALER 64
#define TOP 0xFFFF

#define LOWLIMIT 1000
#define HIGHLIMIT 2000

#define LEFTOFFSET 102.67
#define RIGHTOFFSET 108.74
#define SCALE 2  // accel full-scale, should be 2, 4, or 8
void autonomousWay();
void radioControl();
void radioControlInit()  ;
void PIDwithHeading();
void PIDsameSpeed();
void contactInt();
void afterContact();
void timer5Init(float timeMs);
void timer5Enable();
void timer5Disable();
void stopWheels();
void turnLeft();
void turnRight();
void carBack();
void carForward();
int getColor();
int getCorner();
void RFinit();
bool keyPress(int pin);
void initLSM303(int fs);
float quickHeading();
float getHeading(int * magValue);
float getTiltHeading(int * magValue, float * accelValue);
void getLSM303_mag(int * rawValues);
void getLSM303_accel(int * rawValues);
byte LSM303_read(byte address);
void LSM303_write(byte data, byte address);


TCS230 colorSensor;
TCS230::colorVector<float> nowColor;


myVNH5019Motor leftMotor(1);
myVNH5019Motor rightMotor(2);

MC33926 rollerMotor;


int accel[3];  // we'll store the raw acceleration values here
int mag[3];  // raw magnetometer values stored here
float realAccel[3];  // calculated acceleration values here
float settingHeading;
float headingOffset = 0;
float settingSpeed = SPEEDLIMIT;
bool finishFlag = 0;
bool colorFlag = 0;
bool stopFlag = 0;
bool backFlag = 0;        // tell the car to just go straght, do no care about whether 
                          // the car is going along the fense
bool PIDoneFlag = 1;      // PID 1 or PID 2
bool returnFlag = 0;

bool contactFlag = 0;
int returncounter = 0;
int counter = 0;
int tcnt5 = 0;
bool automaticFlag;    // 1 for automatic, 0 for manually operated
bool straightFlag;     // 1 for going staight, 0 for turning
int corner;            // means which corner the car end up with before execute the returning command
int ch5;

// PID for go straight
float kp1;
float ki1;
float kd1;

// PID parameter for heading
float kp2;
float ki2;
float kd2;

int outputPWMLeft;
int outputPWMRight;
float ek1Left = 0;
float ek2Left = 0;
float ek1Right = 0;
float ek2Right = 0;
float uk1Left = 0;
float uk1Right = 0;

float currentHeading;
float leftSpeed;
float rightSpeed;
float tiltedHeading;
float filterFrequency = 4.0;

FilterOnePole leftlowpassFilter(LOWPASS,filterFrequency);
FilterOnePole rightlowpassFilter(LOWPASS,filterFrequency);
void setup() 
{
	Serial.begin(115200);
    Wire.begin();
    Serial.println("Program Begins"); 
    radioControlInit();
    initLSM303(SCALE);
	leftMotor.initmotor();
	rightMotor.initmotor();
	rollerMotor.initMotor();
	/*colorSensor.initcolor();
	RFinit();
	
	
	while(!keyPress(APIN))
	{
	}
	settingHeading = quickHeading();
    headingOffset = 120 - settingHeading;
	settingHeading = 120;
*/	
	pinMode(LIMITSWITCH,INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(LIMITSWITCH), contactInt, FALLING);     
        rollerMotor.setDirection(1);
	rollerMotor.setSpeed(255);
	rollerMotor.start();
	colorSensor.lightOn();
	timer5Init(INTTIME);   //timer 5 interrupt every 10 ms	
    sei();   //enable global interrupt

}

void loop() 
{
	ch5=pulseIn(CH5,HIGH,25000);
	ch5=map(ch5,LOWLIMIT,HIGHLIMIT,-255,255);
	ch5=constrain(ch5, -255, 255);
    Serial.print("ch5: ");
    Serial.println(ch5);

    if(ch5<50&&ch5>-50)
    {
		ch5=0;
	}
	if (ch5 >= 50)
	{
		autonomousWay();
	}
	else if (ch5 < -50)
	{
		leftSpeed = leftMotor.getSpeed();
		rightSpeed = rightMotor.getSpeed();
		PIDoneFlag = 0;
		timer5Enable();
		radioControl();		
	}
	else
	{
		stopWheels();
		rollerMotor.stop();
		colorSensor.lightOff();
        timer5Disable();		
	}
}

void autonomousWay()
{
	if (contactFlag)
	{
		afterContact();
		
	}
	settingSpeed = SPEEDLIMIT;
	if (!finishFlag)
	{
		if (!colorFlag && !returnFlag)
		{
			if (!backFlag)
			{
				PIDoneFlag = 1;
				tiltedHeading = quickHeading();
		        leftSpeed = leftMotor.getSpeed();
			}
			else
			{
				leftSpeed = leftMotor.getSpeed();
		        rightSpeed = rightMotor.getSpeed();
				PIDoneFlag = 0;
			}
		}
		else
		{
			timer5Disable();
			colorSensor.lightOff();
			backFlag = 1;
			if (getCorner() == 3)
			{
				turnLeft();
				delay(TURNTIME);
				stopWheels();
				carForward();
				returncounter++;
				if (returncounter == 2)
				{
					stopFlag = 1;
				}
			}
			else if (getCorner() == 4)
			{
                turnRight();
                delay(TURNTIME);
                stopWheels();
                carForward();
                stopFlag = 1;				
			}
			colorFlag = 0;
			timer5Enable();
		}
	}
	else
	{
	    stopWheels();
        rollerMotor.stop();	
		timer5Disable();
	}
	return;
}

void radioControl()
{
	int ch1;
	int ch2;
	int ch3;
	int absch3;
	
	PIDoneFlag = 0;	
	ch1=pulseIn(CH1,HIGH,25000);
	ch2=pulseIn(CH2,HIGH,25000);
	ch3=pulseIn(CH3,HIGH,25000);

	ch1=map(ch1,LOWLIMIT,HIGHLIMIT,-255,255);
	ch1=constrain(ch1, -255, 255);
	ch2=map(ch2,LOWLIMIT,HIGHLIMIT,-255,255);
	ch2=constrain(ch2, -255, 255);
	ch3=map(ch3,LOWLIMIT,HIGHLIMIT,-255,255 );
	ch3=constrain(ch3, -255,255);
	Serial.print("ch1 = ");
	Serial.println(ch1);
	Serial.print("ch2 = ");
	Serial.println(ch2);
	Serial.print("ch3 = ");
	Serial.println(ch3);
    absch3 = abs(ch3);
	if(ch1 < 20 && ch1 > -20)
	{
		ch1=0;
	}
	if(ch2 < 50 && ch2 > -50)
	{
		ch2=0;
	}
	if(ch3 < 120 && ch3 > -120)
	{
		ch3=0;
	}

    if(ch1 == 0 && ch2 == 0)
	{
		settingSpeed = 0;
	    stopWheels();
	}	
    else if(ch1 != 0)
	{
		settingSpeed = map(abs(ch1),0,255,0,SPEEDLIMIT);
		leftMotor.setSpeed(ch1);
		rightMotor.setSpeed(ch1);
	}
	else if(ch2 != 0)
	{
		settingSpeed = map(abs(ch2),0,255,0,SPEEDLIMIT);
		leftMotor.setSpeed(-ch2);
		rightMotor.setSpeed(ch2);
	}
	
	if(ch3 == 0)
	{
		rollerMotor.stop();
	}
	else if (ch3 > 0)
	{
		rollerMotor.setDirection(1);
		rollerMotor.setSpeed(absch3);		
	}
	else if (ch3 < 0)
	{
		rollerMotor.setDirection(0);
		rollerMotor.setSpeed(absch3);
	} 
	
	return;	
}

void radioControlInit()  
{
	pinMode(CH1,INPUT);
	pinMode(CH2,INPUT);
	pinMode(CH3,INPUT);
	pinMode(CH5,INPUT);
	return;
}
ISR(TIMER5_OVF_vect)
{
	if(PIDoneFlag)    // PID 1: both direction and straightness
	{
		settingSpeed = SPEEDLIMIT;
		PIDwithHeading();		
	}
	else              // PID 2: only straightness
	{

		PIDsameSpeed();		
	}
	TCNT5 = tcnt5;
}
void PIDwithHeading()
{
	float ukLeft;
	float ukRight;
	float deltaULeft;
	float deltaURight;
	float kpLeft;
	float kiLeft;
	float kdLeft;
	float kpRight;
	float kiRight;
	float kdRight;

    float ekLeft;
    float ekRight;
	
	kpLeft = kp1;
	kiLeft = ki1;
	kdLeft = kd1;
	kpRight = kp2;
	kiRight = ki2;
	kdRight = kd2;
	
	if (outputPWMLeft >= 255 || outputPWMLeft <= 0)
	{
		kiLeft = 0;
	}
	if (outputPWMRight >= 255 || outputPWMRight <= 0)
	{
		kiRight = 0;
	}

	
	currentHeading = tiltedHeading + headingOffset;
	if (currentHeading >= 360)
	{
		currentHeading -= 360;
	}
	else if (currentHeading < 0)
	{
		currentHeading -= 360;
	}
	
	ekLeft = settingSpeed - leftSpeed;
	ekRight = settingHeading - currentHeading;
	
	deltaULeft += kpLeft * (ekLeft - ek1Left);
    deltaULeft += kiLeft * (INTTIME / 1000.0) * ekLeft;
	deltaULeft += kdLeft / (INTTIME / 1000.0) * (ekLeft - 2 * ek1Left + ek2Left);
	
    deltaURight += kpRight * (ekRight - ek1Right);
    deltaURight += kiRight * (INTTIME / 1000.0) * ekRight;
	deltaURight += kdRight / (INTTIME / 1000.0) * (ekRight - 2 * ek1Right + ek2Right);
	
	ukLeft = uk1Left + deltaULeft;
	ukRight = uk1Right + deltaURight;
	
	outputPWMLeft = ukLeft + LEFTOFFSET;
	outputPWMRight = ukRight + RIGHTOFFSET;
	
	if (outputPWMLeft >= 255)
	{
		outputPWMLeft = 255;
	}
	else if(outputPWMLeft <= 0)
	{
		outputPWMLeft = 0;
	}
	
    if (outputPWMRight >= 255)
	{
		outputPWMRight = 255;
	}
	else if(outputPWMRight <= 0)
	{
		outputPWMRight = 0;
	}
	
	ek1Left = ekLeft;
	ek2Left = ek1Left;
	
	ek1Right = ekRight;
	ek2Right = ek1Right;
	
	uk1Left = ukLeft;
	uk1Right = ukRight;
	leftMotor.outputPWM(outputPWMLeft);
	rightMotor.outputPWM(outputPWMRight);		
}
void PIDsameSpeed()
{
	float ukLeft;
	float ukRight;
	float deltaULeft;
	float deltaURight;
	float kpLeft;
	float kiLeft;
	float kdLeft;
	float kpRight;
	float kiRight;
	float kdRight;

        float ekLeft;
        float ekRight;
	
	kpLeft = kp1;
	kiLeft = ki1;
	kdLeft = kd1;
	kpRight = kp1;
	kiRight = ki1;
	kdRight = kd1;
	
	if (outputPWMLeft >= 255 || outputPWMLeft <= 0)
	{
		kiLeft = 0;
	}
	if (outputPWMRight >= 255 || outputPWMRight <= 0)
	{
		kiRight = 0;
	}

	
	if (settingSpeed >= SPEEDLIMIT)
	{
		settingSpeed = SPEEDLIMIT;		
	}
	ekLeft = settingSpeed - leftSpeed;
	ekRight = settingSpeed - rightSpeed;
	
	deltaULeft += kpLeft * (ekLeft - ek1Left);
    deltaULeft += kiLeft * (INTTIME / 1000.0) * ekLeft;
	deltaULeft += kdLeft / (INTTIME / 1000.0) * (ekLeft - 2 * ek1Left + ek2Left);
	
    deltaURight += kpRight * (ekRight - ek1Right);
    deltaURight += kiRight * (INTTIME / 1000.0) * ekRight;
	deltaURight += kdRight / (INTTIME / 1000.0) * (ekRight - 2 * ek1Right + ek2Right);
	
	ukLeft = uk1Left + deltaULeft;
	ukRight = uk1Right + deltaURight;
	
	outputPWMLeft = ukLeft + LEFTOFFSET;
	outputPWMRight = ukRight + RIGHTOFFSET;
	
	if (outputPWMLeft >= 255)
	{
		outputPWMLeft = 255;
	}
	else if(outputPWMLeft <= 0)
	{
		outputPWMLeft = 0;
	}
	
    if (outputPWMRight >= 255)
	{
		outputPWMRight = 255;
	}
	else if(outputPWMRight <= 0)
	{
		outputPWMRight = 0;
	}
	
	ek1Left = ekLeft;
	ek2Left = ek1Left;
	
	ek1Right = ekRight;
	ek2Right = ek1Right;
	
	uk1Left = ukLeft;
	uk1Right = ukRight;
	leftMotor.outputPWM(outputPWMLeft);
	rightMotor.outputPWM(outputPWMRight);	
}
void contactInt()
{
	contactFlag = 1;
}

void afterContact()
{
	timer5Disable();
	carBack();
	delay(BACKTIME);
	stopWheels();
	if (!stopFlag)
	{
		if (returncounter != 1)
		{
			if (getColor() != 1 && getColor() != 3)
			{
				counter++;				
				if (counter % 2 == 0)
				{
					turnLeft();
					delay(TURNTIME);
					stopWheels();
					carForward();
					delay(FORWARDTIME);
					stopWheels();
					turnLeft();
					delay(TURNTIME);
					stopWheels();
					carForward();
					
				}
				else
				{
					turnRight();
					delay(TURNTIME);
					stopWheels();
					carForward();
					delay(FORWARDTIME);
					stopWheels();
					turnRight();
					delay(TURNTIME);
					stopWheels();
					carForward();
				}
                timer5Enable();
			}
			else
			{
                colorFlag = 1;				
			}
		}
		else
		{
			returnFlag = 1;
		}
	}
	else
	{
        stopWheels();
        finishFlag = 1;
	}
	contactFlag = 0;

}

void timer5Init(float timeMs)    // ms
{
  timer5Enable();
  TCCR5A &= ~((1<<WGM51)|(1<<WGM50));
  TCCR5B &= ~((1<<WGM53)|(1<<WGM52)); 
  TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
  TCCR5B &= ~(1<<CS52);
  tcnt5 = TOP + 1 - timeMs / 1000.0 * (CPUFREQUENCY / PRESCALER);
  TCNT5 = tcnt5;
  return;
}

void timer5Enable()
{
	TIMSK5 |= (1<<TOIE5);     // enable timer
	TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
    TCCR5B &= ~(1<<CS52);
	return;
}

void timer5Disable()
{
	TIMSK5 &= ~(1<<TOIE5);
	TCCR5B &= ~((1<<CS52)|(1<<CS51)|(1<<CS50));
	return;
}



void stopWheels()
{
	leftMotor.stop();
	rightMotor.stop();
	return;
}

void turnLeft()
{
	leftMotor.forward();
	rightMotor.reverse();
	return;
}

void turnRight()
{
	leftMotor.reverse();
	rightMotor.forward();
	return;
}

void carBack()
{
	leftMotor.reverse();
	rightMotor.reverse();
	return;
}

void carForward()
{
	leftMotor.forward();
	rightMotor.forward();
	return;
}

int getColor()
{
	int color;
	
	colorSensor.colorGetColorValue(&nowColor);
	
	if (nowColor.red > nowColor.green && nowColor.red > nowColor.blue)
	{
		color = 1;
	}
	else if (nowColor.green > nowColor.red && nowColor.green > nowColor.blue)
	{
		color = 2;
	}
	else if (nowColor.blue > nowColor.red && nowColor.blue > nowColor.green)
	{
		color = 3;  
	}
	else
	{
		color = 4;
	}	
	return(color);
}
int getCorner()
{
	int corner;
		
	// red : corner 3
	// blue: corner 4
	if (getColor() == 1 )
	{
		corner = 3;
	}
	else if (getColor() ==3)
	{
		corner = 4;
	}
	return(corner);
}

void RFinit()
{
	pinMode(APIN,INPUT);
	pinMode(BPIN,INPUT);
	pinMode(CPIN,INPUT);
	pinMode(DPIN,INPUT);
	return;
}

bool keyPress(int pin)
{
    bool flag = 0;	
	if (digitalRead(pin) == HIGH)
	{
		delay(20);
		if (digitalRead(pin) == HIGH)
		{
		// Run the program	
			flag = 1;
			
			while(digitalRead(pin) == HIGH)
			{                     // wait for release the key
			};
		}		
	}
	return(flag);
}









void initLSM303(int fs)
{
  LSM303_write(0x27, CTRL_REG1_A);  // 0x27 = normal power mode, all accel axes on
  if ((fs==8)||(fs==4))
    LSM303_write((0x00 | (fs-fs/2-1)<<4), CTRL_REG4_A);  // set full-scale
  else
    LSM303_write(0x00, CTRL_REG4_A);
  LSM303_write(0x14, CRA_REG_M);  // 0x14 = mag 30Hz output rate
  LSM303_write(0x00, MR_REG_M);  // 0x00 = continouous conversion mode
}

float quickHeading()
{
   getLSM303_accel(accel);  // get the acceleration values and store them in the accel array
  while(!(LSM303_read(SR_REG_M) & 0x01))
    ;  // wait for the magnetometer readings to be ready
  getLSM303_mag(mag);  // get the magnetometer values, store them in mag
  //printValues(mag, accel);  // print the raw accel and mag values, good debugging
  
  for (int i=0; i<3; i++)
    realAccel[i] = accel[i] / pow(2, 15) * SCALE;  // calculate real acceleration values, in units of g
  
  /* print both the level, and tilt-compensated headings below to compare */
  //Serial.print(getHeading(mag), 3);  // this only works if the sensor is level
 // Serial.print("\t\t");  // print some tabs
 // Serial.println(getTiltHeading(mag, realAccel), 3);  // see how awesome tilt compensation is?!
  //delay(100);  // delay for serial readability 
  return(getTiltHeading(mag,realAccel));  
}

float getHeading(int * magValue)
{
  // see section 1.2 in app note AN3192
  float heading = 180*atan2(magValue[Y], magValue[X])/PI;  // assume pitch, roll are 0
  
  if (heading <0)
    heading += 360;
  
  return heading;
}

float getTiltHeading(int * magValue, float * accelValue)
{
  // see appendix A in app note AN3192 
  float pitch = asin(-accelValue[X]);
  float roll = asin(accelValue[Y]/cos(pitch));
  
  float xh = magValue[X] * cos(pitch) + magValue[Z] * sin(pitch);
  float yh = magValue[X] * sin(roll) * sin(pitch) + magValue[Y] * cos(roll) - magValue[Z] * sin(roll) * cos(pitch);
  float zh = -magValue[X] * cos(roll) * sin(pitch) + magValue[Y] * sin(roll) + magValue[Z] * cos(roll) * cos(pitch);

  float heading = 180 * atan2(yh, xh)/PI;
  if (yh >= 0)
    return heading;
  else
    return (360 + heading);
}

void getLSM303_mag(int * rawValues)
{
  Wire.beginTransmission(LSM303_MAG);
  Wire.write(OUT_X_H_M);
  Wire.endTransmission();
  Wire.requestFrom(LSM303_MAG, 6);
  for (int i=0; i<3; i++)
    rawValues[i] = (Wire.read() << 8) | Wire.read();
}

void getLSM303_accel(int * rawValues)
{
  rawValues[Z] = ((int)LSM303_read(OUT_X_L_A) << 8) | (LSM303_read(OUT_X_H_A));
  rawValues[X] = ((int)LSM303_read(OUT_Y_L_A) << 8) | (LSM303_read(OUT_Y_H_A));
  rawValues[Y] = ((int)LSM303_read(OUT_Z_L_A) << 8) | (LSM303_read(OUT_Z_H_A));  
  // had to swap those to right the data with the proper axis
}

byte LSM303_read(byte address)
{
  byte temp;
  
  if (address >= 0x20)
    Wire.beginTransmission(LSM303_ACC);
  else
    Wire.beginTransmission(LSM303_MAG);
    
  Wire.write(address);
  
  if (address >= 0x20)
    Wire.requestFrom(LSM303_ACC, 1);
  else
    Wire.requestFrom(LSM303_MAG, 1);
  while(!Wire.available())
    ;
  temp = Wire.read();
  Wire.endTransmission();
  
  return temp;
}

void LSM303_write(byte data, byte address)
{
  if (address >= 0x20)
    Wire.beginTransmission(LSM303_ACC);
  else
    Wire.beginTransmission(LSM303_MAG);
    
  Wire.write(address);
  Wire.write(data);
  Wire.endTransmission();
}
	
