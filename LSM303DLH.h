#ifndef LSM303DLH_H
#define LSM303DLH_H

/* LSM303DLH Example Code
 
   LSM303 Breakout ---------- Arduino(UNO)
         Vin                   5V
         GND                   GND
         SDA                   A4
         SCL                   A5
*/
#include <Arduino.h>
#include <Wire.h>
#include <math.h>

 
/* LSM303 Address definitions */
#define LSM303_MAG  0x1E  // assuming SA0 grounded
#define LSM303_ACC  0x18  // assuming SA0 grounded
 
#define X 0
#define Y 1
#define Z 2
 
/* LSM303 Register definitions */
#define CTRL_REG1_A 0x20
#define CTRL_REG2_A 0x21
#define CTRL_REG3_A 0x22
#define CTRL_REG4_A 0x23
#define CTRL_REG5_A 0x24
#define HP_FILTER_RESET_A 0x25
#define REFERENCE_A 0x26
#define STATUS_REG_A 0x27
#define OUT_X_L_A 0x28
#define OUT_X_H_A 0x29
#define OUT_Y_L_A 0x2A
#define OUT_Y_H_A 0x2B
#define OUT_Z_L_A 0x2C
#define OUT_Z_H_A 0x2D
#define INT1_CFG_A 0x30
#define INT1_SOURCE_A 0x31
#define INT1_THS_A 0x32
#define INT1_DURATION_A 0x33
#define CRA_REG_M 0x00
#define CRB_REG_M 0x01
#define MR_REG_M 0x02
#define OUT_X_H_M 0x03
#define OUT_X_L_M 0x04
#define OUT_Y_H_M 0x05
#define OUT_Y_L_M 0x06
#define OUT_Z_H_M 0x07
#define OUT_Z_L_M 0x08
#define SR_REG_M 0x09
#define IRA_REG_M 0x0A
#define IRB_REG_M 0x0B
#define IRC_REG_M 0x0C
 
class LSM303DFROBOT
{
	public:
	LSM303DFROBOT(int scale);
	void initLSM303();
	float getHeading();
	float getTiltHeading();
	void getLSM303_mag();
	void getLSM303_accel();
	byte LSM303_read(byte address);
	void LSM303_write(byte data, byte address);
	void quickHeading(bool toDisplay);    // a qucik way to get the heading info
	float getTiltHeadingNum();
	
	private:	
	int SCALE;
	float nowHeading;
	float nowTiltHeading;
	int accelValue[3];
	int magValue[3];
	float realAccel[3];	
};







#endif
