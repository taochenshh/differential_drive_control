#include "MC33926.h"

MC33926::MC33926()
{
	IN1 = 27;
	IN2 = 26;
	D2 = 28;
	EN = 30;
	
}
MC33926::MC33926(int in1, int in2, int d2, int en)
{
	IN1 = in2;
	IN2 = in1;
	D2 = d2;
	EN = en;
}

void MC33926::initMotor()
{
	pinMode(IN1, OUTPUT);
	pinMode(IN2, OUTPUT);
	pinMode(D2, OUTPUT);
	pinMode(EN, OUTPUT);
	stop();
	return;
}
void MC33926::setDirection(bool direction)
{
	if (direction)
	{
		digitalWrite(IN1, HIGH);
		digitalWrite(IN2, LOW);
	}
	else
	{
		digitalWrite(IN1, LOW);
		digitalWrite(IN2, HIGH);
	}	
	return;
}
void MC33926::setSpeed(uint8_t speed)
{
	analogWrite(D2, speed);
	return;
}
void MC33926::start()
{
	digitalWrite(EN,HIGH);
  digitalWrite(D2,HIGH);
  return;
}
void MC33926::stop()
{
	analogWrite(D2, 0);
	digitalWrite(EN,LOW);
	return;
}
