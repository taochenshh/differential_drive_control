#define APIN 37
#define BPIN 38
#define CPIN 39
#define DPIN 40

void setup() 
{
	Serial.begin(9600);
	RFinit();
}

void loop() 
{
	if (keyPress(APIN) == 1)
	{
		Serial.println("A key pressed.");
	}
	if (keyPress(BPIN) == 1)
	{
		Serial.println("B key pressed.");
	}
	if (keyPress(CPIN) == 1)
	{
		Serial.println("C key pressed.");
	}
	if (keyPress(DPIN) == 1)
	{
		Serial.println("D key pressed.");
	}
}

void RFinit()
{
	pinMode(APIN,INPUT);
	pinMode(BPIN,INPUT);
	pinMode(CPIN,INPUT);
	pinMode(DPIN,INPUT);
	return;
}

bool keyPress(int pin)
{
    bool flag = 0;	
	if (digitalRead(pin) == HIGH)
	{
		delay(20);
		if (digitalRead(pin) == HIGH)
		{
		// Run the program	
			flag = 1;
			
			while(digitalRead(pin) == HIGH)
			{                     // wait for release the key
			};
		}		
	}
	return(flag);
}
	

