#ifndef MYVNH5019MOTOR_H
#define MYVNH5019MOTOR_H

#include <Arduino.h>
class myVNH5019Motor
{
	public:
	myVNH5019Motor(unsigned char choice);	
	void initmotor();
	void setDirection(bool direction);
	void setSpeed(int speed);
	void outputPWM(int pwm);
	void stop();
	void forward();
	void reverse();
	
	unsigned int getCurrent();
    float getSpeed();
	unsigned char hallSensor;
	private:
    unsigned char INA;
	unsigned char INB;
	unsigned char ENDIAG;
	unsigned char CS;
	unsigned char PWMPIN;
	
	
};

#endif
