#include "myVNH5019motor.h"
#include <Filters.h>

myVNH5019Motor leftMotor(1);
myVNH5019Motor rightMotor(2);

#define TOP 0xFFFF
#define CPUFREQUENCY 16000000
#define PRESCALER 64
#define INTTIME 10

void timer5Init(float);
void timer5Enable();
void timer5Disable();

int tcnt5;
volatile unsigned int leftCount = 0;
volatile unsigned int rightCount = 0;

float filterFrequency = 2.0;

FilterOnePole leftlowpassFilter(LOWPASS,filterFrequency);
FilterOnePole rightlowpassFilter(LOWPASS,filterFrequency);
volatile float leftSpeed;
volatile float rightSpeed;
volatile float filtedLeftSpeed;
volatile float filtedRightSpeed;
volatile float leftSpeedShared;
void setup()
{
	Serial.begin(115200);
	delay(2000);
	Serial.println("Communication Successful");
	leftMotor.initmotor();
	rightMotor.initmotor();
	leftMotor.setDirection(0);
	rightMotor.setDirection(0);
        timer3Init();
    attachInterrupt(digitalPinToInterrupt(leftMotor.hallSensor), leftMInt, RISING);
//	attachInterrupt(digitalPinToInterrupt(rightMotor.hallSensor), rightMInt, RISING);
	//timer5Init(INTTIME);
	sei(); 
}

void loop()
{
  int i = 0;
  for (i = 90; i <= 255; i += 5)
  {
    leftMotor.outputPWM(i);
   // rightMotor.outputPWM(i);
    delay(2000);
    noInterrupts();	
    Serial.print(i);
    Serial.print("    ");
    Serial.print(leftSpeed);
    Serial.print("    ");
    Serial.println(filtedLeftSpeed);
/*	Serial.print("    ");
    Serial.print(rightSpeed);
	Serial.print("    ");
    Serial.println(filtedRightSpeed);*/
    interrupts();
  }
  
  leftMotor.outputPWM(0);
  rightMotor.outputPWM(0);
  delay(50000);
}

void leftMInt()
{
  TCNT3=0x0000;  
  while(digitalRead(leftMotor.hallSensor) == HIGH)
  {
  }
  leftSpeedShared = 1.0 / (TCNT3 / 16000000.0 * 2) / 16.0 * 60.0 / 19.0 ;
  Serial.println(leftSpeedShared);
}

void rightMInt()
{
	rightCount++;
}

ISR(TIMER5_OVF_vect)
{
    TCNT5 = tcnt5; 
	//leftSpeed = leftCount * 1000.0 / INTTIME / 19.0 * 60.0 / 16.0;
       leftSpeed = leftSpeedShared;	
leftlowpassFilter.input(leftSpeed);
	filtedLeftSpeed = leftlowpassFilter.output();		
/*	rightSpeed = rightCount * 1000.0 / INTTIME / 19.0 * 60.0 / 16.0;
	rightlowpassFilter.input(rightSpeed);
	filtedRightSpeed = rightlowpassFilter.output();
	*/
	leftCount = 0;
	rightCount = 0;
}

void timer5Init(float timeMs)    // ms
{
	TCCR5A &= ~((1<<WGM51)|(1<<WGM50));
	TCCR5B &= ~((1<<WGM53)|(1<<WGM52)); 
	TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
	TCCR5B &= ~(1<<CS52);
	tcnt5 = TOP + 1 - timeMs / 1000.0 * (CPUFREQUENCY / PRESCALER);
	TCNT5 = tcnt5;
	timer5Enable();
	return;
}
void timer3Init(void)
{
  /*no prescaling */
  TCCR3A=0x00;
  TCCR3B=(1<<CS30);
  TCNT3=0x00;
}
void timer5Enable()
{
	TIMSK5 |= (1<<TOIE5);     // enable timer
	TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
	TCCR5B &= ~(1<<CS52);
	return;
}

void timer5Disable()
{
	TIMSK5 &= ~(1<<TOIE5);
	TCCR5B &= ~((1<<CS52)|(1<<CS51)|(1<<CS50));
	return;
}

