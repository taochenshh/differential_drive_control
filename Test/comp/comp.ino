#include "LSM303DLH.h"
#include <Wire.h>

float tilt;
LSM303 compass(2);
void setup()
{
  Serial.begin(9600);
  Wire.begin();  // Start up I2C, required for LSM303 communication
  Serial.println("Communication begin:");     
  compass.initLSM303();
}

void loop() 
{
  compass.quickHeading();
  tilt = compass.getNowHeading();
  Serial.print("loop : ");
  Serial.println(tilt);

}
