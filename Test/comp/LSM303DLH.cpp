#include "LSM303DLH.h"
#include <Arduino.h>

LSM303::LSM303(int scl)
{
	SCALE = scl;
	Wire.begin();
}

void LSM303::initLSM303()
{
	LSM303_write(0x27, CTRL_REG1_A);  // 0x27 = normal power mode, all accel axes on
	if ((SCALE==8)||(SCALE==4))
		LSM303_write((0x00 | (SCALE-SCALE/2-1)<<4), CTRL_REG4_A);  // set full-scale
	else
		LSM303_write(0x00, CTRL_REG4_A);
	LSM303_write(0x14, CRA_REG_M);  // 0x14 = mag 30Hz output rate
	LSM303_write(0x00, MR_REG_M);  // 0x00 = continouous conversion mode
}
void LSM303::quickHeading()
{
   getLSM303_accel();  // get the acceleration values and store them in the accel array
  while(!(LSM303_read(SR_REG_M) & 0x01))
    ;  // wait for the magnetometer readings to be ready
  getLSM303_mag();  // get the magnetometer values, store them in mag
  //printValues(mag, accel);  // print the raw accel and mag values, good debugging
  
  for (int i=0; i<3; i++)
    realAccel[i] = accel[i] / pow(2, 15) * SCALE;  // calculate real acceleration values, in units of g
    nowTiltHeading = getTiltHeading();
}
float LSM303::getNowHeading()
{
  return(nowTiltHeading);
}
float LSM303::getHeading()
{
	// see section 1.2 in app note AN3192
	float heading = 180*atan2(mag[Y], mag[X])/PI;  // assume pitch, roll are 0

	if (heading <0)
		heading += 360;

	return heading;
}

float LSM303::getTiltHeading()
{
	// see appendix A in app note AN3192 
	float pitch = asin(-realAccel[X]);
	float roll = asin(realAccel[Y]/cos(pitch));

	float xh = mag[X] * cos(pitch) + mag[Z] * sin(pitch);
	float yh = mag[X] * sin(roll) * sin(pitch) + mag[Y] * cos(roll) - mag[Z] * sin(roll) * cos(pitch);
	float zh = -mag[X] * cos(roll) * sin(pitch) + mag[Y] * sin(roll) + mag[Z] * cos(roll) * cos(pitch);

	float heading = 180 * atan2(yh, xh)/PI;
	if (yh >= 0)
		return heading;
	else
		return (360 + heading);
}

void LSM303::getLSM303_mag()
{
	Wire.beginTransmission(LSM303_MAG);
	Wire.write(OUT_X_H_M);
	Wire.endTransmission();
	Wire.requestFrom(LSM303_MAG, 6);
	for (int i=0; i<3; i++)
		mag[i] = (Wire.read() << 8) | Wire.read();
}

void LSM303::getLSM303_accel()
{
	accel[Z] = ((int)LSM303_read(OUT_X_L_A) << 8) | (LSM303_read(OUT_X_H_A));
	accel[X] = ((int)LSM303_read(OUT_Y_L_A) << 8) | (LSM303_read(OUT_Y_H_A));
	accel[Y] = ((int)LSM303_read(OUT_Z_L_A) << 8) | (LSM303_read(OUT_Z_H_A));  
	// had to swap those to right the data with the proper axis
}

byte LSM303::LSM303_read(byte address)
{
	byte temp;

	if (address >= 0x20)
		Wire.beginTransmission(LSM303_ACC);
	else
		Wire.beginTransmission(LSM303_MAG);

	Wire.write(address);

	if (address >= 0x20)
		Wire.requestFrom(LSM303_ACC, 1);
	else
		Wire.requestFrom(LSM303_MAG, 1);
	while(!Wire.available())
	;
	temp = Wire.read();
	Wire.endTransmission();

	return temp;
}

void LSM303::LSM303_write(byte data, byte address)
{
	if (address >= 0x20)
		Wire.beginTransmission(LSM303_ACC);
	else
		Wire.beginTransmission(LSM303_MAG);

	Wire.write(address);
	Wire.write(data);
	Wire.endTransmission();
}

