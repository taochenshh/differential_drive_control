#ifndef TCS230_H
#define TCS230_H

#include <Arduino.h>
#define CLEAR 0
#define RED 1
#define GREEN 2
#define BLUE 3

class TCS230
{
	public:	
	
	template <typename T>
	struct colorVector
	{
		T red;
		T green;
		T blue;
	};
	TCS230();
	void initcolor(int num);
	float colorGetFrequency(int filter);
	void colorSetFilter(int filter);
	void colorBlackBalance();
	void colorWhiteBalance();
	void lightOn();
	void lightOff();
	void setBlackColor(float redfreq, float greenfreq, float bluefreq);
	void setWhiteColor(float redfreq, float greenfreq, float bluefreq);
	float colorConvertFreqToData(float freq, int filter);
	void colorGetColorValue(colorVector<float>* currentColor);
	
	private:
	uint8_t COLORSENSOR_S0;
    uint8_t COLORSENSOR_S1;
	uint8_t COLORSENSOR_S2;
	uint8_t COLORSENSOR_S3;
	uint8_t COLORSENSOR_LED;
	uint8_t COLORSENSOR_OUT;
  float scale;
	
	colorVector<float> blackBalance;
	colorVector<float> whiteBalance;
	
};
#endif
