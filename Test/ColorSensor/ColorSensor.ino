#include "TCS230.h"

TCS230 colorSensor;
TCS230::colorVector<float> nowColor;

int color;
void setup() 
{
    Serial.begin(9600);
	  colorSensor.initcolor(1);
	 colorSensor.lightOn();
  delay(2000);
  Serial.println("BEGIN");
    colorSensor.colorBlackBalance();
    delay(5000);
    colorSensor.colorWhiteBalance();
    delay(5000);
}

void loop() 
{
	color = getColor();
  delay(3000);
	if (color == 1)
	{
		Serial.println("RED");
	}
	else if (color == 2)
	{
		Serial.println("GREEN");
	}
	else if (color == 3)
	{
		Serial.println("BLUE");
	}
	else
	{
		Serial.println("Not RED, GREEN, or BLUE");
	}	
}	

int getColor()
{
	int color;
	
	colorSensor.colorGetColorValue(&nowColor);
  Serial.print("red: ");
  Serial.println(nowColor.red );
    Serial.print("green: ");
  Serial.println(nowColor.green );
    Serial.print("blue: ");
  Serial.println(nowColor.blue );
	
	if (fabs(nowColor.red - 255) < 30 && fabs(nowColor.green) < 30 && fabs(nowColor.blue) < 30)
	{
		color = 1;
	}
	else if (fabs(nowColor.red) < 30 && fabs(nowColor.green - 255) < 30 && fabs(nowColor.blue) < 30)
	{
		color = 2;
	}
	else if (fabs(nowColor.red) < 30 && fabs(nowColor.green) < 30 && fabs(nowColor.blue - 255) < 30)
	{
		color = 3;  
	}
	else
	{
		color = 4;
	}	
	return(color);
}
  


