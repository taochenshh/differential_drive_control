#ifndef MC33926_H
#define MC33926_H

#include <Arduino.h>
class MC33926
{
	public:
        MC33926();
	MC33926(int in1, int in2, int d2, int en);
	void initMotor();
	void setDirection(bool direction);
	void setSpeed(uint8_t speed);
	void start();
	void stop();
	
	private:
	uint8_t IN1;
	uint8_t IN2;
	uint8_t D2;
	uint8_t EN;	
};

#endif
