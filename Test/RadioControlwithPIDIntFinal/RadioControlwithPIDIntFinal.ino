#include "MC33926.h"
#include "myVNH5019motor.h"
#include <Filters.h>

#define CH1 3
#define CH2 20   
#define CH3 21

#define INTTIME 20
#define SPEEDLIMIT 70   // 60 ok
#define CPUFREQUENCY 16000000
#define PRESCALER 64
#define TOP 0xFFFF

#define LEFTOFFSET 94.91
#define RIGHTOFFSET 126.09


myVNH5019Motor leftMotor(1);
myVNH5019Motor rightMotor(2);
MC33926 rollerMotor;

volatile int ch1Shared;
volatile int ch2Shared;
volatile int ch3Shared;

volatile float settingSpeed = SPEEDLIMIT ;
int outputPWMLeft;
int outputPWMRight;
float ek1Left = 0;
float ek2Left = 0;
float ek1Right = 0;
float ek2Right = 0;
float uk1Left = 0;
float uk1Right = 0;

volatile unsigned int leftCount = 0;
volatile unsigned int rightCount = 0;
volatile uint8_t radioFlagsShared = 0;

float kp1 = 10;
float ki1 = 8;
float kd1 = 0.02;
float kp2 = 12;
float ki2 = 8;
float kd2 = 0.02;
int tcnt5;
float filterFrequency = 2.0;
volatile unsigned long t;
FilterOnePole leftlowpassFilter(LOWPASS,filterFrequency);
FilterOnePole rightlowpassFilter(LOWPASS,filterFrequency);

void setup() 
{
	Serial.begin(115200);
	radioControlInit();
	leftMotor.initmotor();
	rightMotor.initmotor();
	rollerMotor.initMotor();
	attachInterrupt(digitalPinToInterrupt(CH1), radioCh1, RISING);
	attachInterrupt(digitalPinToInterrupt(CH2), radioCh2, RISING);
	attachInterrupt(digitalPinToInterrupt(CH3), radioCh3, RISING);
	attachInterrupt(digitalPinToInterrupt(leftMotor.hallSensor), leftMInt, RISING);
	attachInterrupt(digitalPinToInterrupt(rightMotor.hallSensor), rightMInt, RISING);
    timer3Init();
	timer5Init(INTTIME);   
	timer5Enable();
	sei();   //enable global interrupt
	t = millis();
}

void loop() 
{
  radioControl();
}

void radioCh1()
{
  float x;
  TCNT3=0x0000;  
  while(digitalRead(CH1) == HIGH)
  {
  }
  x = TCNT3 / 16.0;
  if (x > 1200 && x < 1900)
  {
    ch1Shared = TCNT3 / 16.0;
  }
}
void radioCh2()
{
  float x;
  TCNT3=0x0000;  
  while(digitalRead(CH2) == HIGH)
  {
  }
  x = TCNT3 / 16.0;
  if ( x > 1200 && x < 2000)
  {
    ch2Shared = TCNT3 / 16.0;
  }
}

void radioCh3()
{	
  float x;
   TCNT3=0x0000;  
  while(digitalRead(CH3) == HIGH)
  {
  }
  x = TCNT3 / 16.0;
  if( x > 1200 && x < 2000)
  {
    ch3Shared = x;
  }
}

void radioControl()
{
	int ch1;
	int ch2;
	int ch3;

	noInterrupts();
	ch1 = ch1Shared;
	ch2 = ch2Shared;
	ch3 = ch3Shared;	
	interrupts();
//Serial.print("*********************");
/*	Serial.print(ch1);
	Serial.print("   ");
	Serial.print(ch2);
	Serial.print("   ");
	Serial.println(ch3);*/

	ch1=map(ch1,1200,1800,255,-255);

	if(ch1 < 50 && ch1 > -50)
	{
		ch1=0;
	}
	
	if(ch1 > 0)
	{
		ch1 = ch1 / 20 * 20 + 10;
	}
	else if (ch1 < 0)
	{
		ch1 = ch1 / 20 * 20 - 10;
	}
	ch1=constrain(ch1, -255, 255);

	ch2=map(ch2,1200,1900,-255,255);			
	if(ch2 < 50 && ch2 > -50)
	{
		ch2=0;
	}
	if(ch2 > 0)
	{
		ch2 = ch2 / 20 * 20 + 10;
	}
	else if (ch2 < 0)
	{
		ch2 = ch2 / 10 * 10 - 10;
	}		
	ch2=constrain(ch2, -255, 255);

	ch3=map(ch3,1200,1920,-255,255 );	
	if(ch3 < 180 && ch3 > -180)
	{
		ch3=0;
	}	
        ch3=constrain(ch3, -255,255);	

     //Serial.print("====================");
/*	Serial.print(ch1);
	Serial.print("   ");
	Serial.print(ch2);
	Serial.print("   ");
	Serial.println(ch3);*/
	
	if(ch1 == 0 && ch2 == 0)
	{
		noInterrupts();
		settingSpeed = 0;
		interrupts();
		stopWheels();
	}	
	else if(ch1 != 0)
	{
		noInterrupts();
		settingSpeed = map(abs(ch1),0,255,0,SPEEDLIMIT);
//Serial.print("CH1  ");
//Serial.println(settingSpeed);
		interrupts();
		if(ch1 > 0)
		{
			leftMotor.setDirection(0);
			rightMotor.setDirection(0);
		}
		else
		{
			leftMotor.setDirection(1);
			rightMotor.setDirection(1);
		}
		//leftMotor.setSpeed(ch1);
		//rightMotor.setSpeed(ch1);
	}
	else if(ch2 != 0)
	{
		noInterrupts();
		settingSpeed = map(abs(ch2),0,255,0,SPEEDLIMIT * 3 / 5);  //36
//Serial.print("CH2  ");
//Serial.println(settingSpeed);
		interrupts();
		if(ch2 > 0)
		{
			leftMotor.setDirection(0);
			rightMotor.setDirection(1);			
		}
		else
		{
			leftMotor.setDirection(1);
			rightMotor.setDirection(0);	
		}
		//leftMotor.setSpeed(-ch2);
		//rightMotor.setSpeed(ch2);
	}	
	
	if(ch3 == 0)
	{
		rollerMotor.stop();
	}
	else if (ch3 > 0)
	{
		rollerMotor.setDirection(0);
		rollerMotor.setSpeed(255);	
		rollerMotor.start();		
	}
	else if (ch3 < 0)
	{
		rollerMotor.setDirection(1);
		rollerMotor.setSpeed(255);
		rollerMotor.start();
	} 	
			
	return;	
}

void leftMInt()
{
	leftCount++;
}

void rightMInt()
{
	rightCount++;
}

ISR(TIMER5_OVF_vect)
{
	float ukLeft;
	float ukRight;
	float deltaULeft;
	float deltaURight;
	float kpLeft;
	float kiLeft;
	float kdLeft;
	float kpRight;
	float kiRight;
	float kdRight;
	float leftSpeed;
	float rightSpeed;
	float ekLeft;
	float ekRight;
 
    TCNT5 = tcnt5; 	
	kpLeft = kp1;
	kiLeft = ki1;
	kdLeft = kd1;
	kpRight = kp2;
	kiRight = ki2;
	kdRight = kd2;

    // PID control
	if (outputPWMLeft >= 255 || outputPWMLeft <= 0)   // Anti-Windup
	{
		kiLeft = 0;
	}
	if (outputPWMRight >= 255 || outputPWMRight <= 0)
	{
		kiRight = 0;
	}	
	leftSpeed = leftCount * 1000.0 / INTTIME / 19.0 * 60.0 / 16.0;
    leftlowpassFilter.input(leftSpeed);  // use low pass filter to filter the incoming signal
    leftSpeed  = leftlowpassFilter.output();		
	rightSpeed = rightCount * 1000.0 / INTTIME / 19.0 * 60.0 / 16.0;
    rightlowpassFilter.input(rightSpeed);
    rightSpeed = rightlowpassFilter.output();
	

	if (settingSpeed >= SPEEDLIMIT)
	{
		settingSpeed = SPEEDLIMIT;		
	}
	ekLeft = settingSpeed - leftSpeed;
	ekRight = settingSpeed - rightSpeed;
	
	
	deltaULeft += kpLeft * (ekLeft - ek1Left);
    deltaULeft += kiLeft * ( INTTIME / 1000.0) * ekLeft;
	deltaULeft += kdLeft / (INTTIME / 1000.0) * (ekLeft - 2 * ek1Left + ek2Left);
	
    deltaURight += kpRight * (ekRight - ek1Right);
    deltaURight += kiRight * (INTTIME / 1000.0) * ekRight;
	deltaURight += kdRight / (INTTIME / 1000.0) * (ekRight - 2 * ek1Right + ek2Right);
	
	ukLeft = uk1Left + deltaULeft;
	ukRight = uk1Right + deltaURight;

	outputPWMLeft = ukLeft + LEFTOFFSET;
	outputPWMRight = ukRight + RIGHTOFFSET;
	
	if (outputPWMLeft >= 255)   // Saturation
	{
		outputPWMLeft = 255;
	}
	else if(outputPWMLeft <= 0)
	{
		outputPWMLeft = 0;
	}
	
    if (outputPWMRight >= 255)
	{
		outputPWMRight = 255;
	}
	else if(outputPWMRight <= 0)
	{
		outputPWMRight = 0;
	}
	
	ek1Left = ekLeft;
	ek2Left = ek1Left;
	
	ek1Right = ekRight;
	ek2Right = ek1Right;
	
	uk1Left = ukLeft;
	uk1Right = ukRight;
	leftMotor.outputPWM(outputPWMLeft);
	rightMotor.outputPWM(outputPWMRight);	
	leftCount = 0;
	rightCount = 0;	
}

void timer3Init(void)
{
  /*no prescaling */
  TCCR3A=0x00;
  TCCR3B=(1<<CS30);
  TCNT3=0x00;
}
void radioControlInit()  
{
  pinMode(CH1,INPUT);
  pinMode(CH2,INPUT);
  pinMode(CH3,INPUT);
  return;
}

void timer5Init(float timeMs)    // ms
{
  timer5Enable();
  TCCR5A &= ~((1<<WGM51)|(1<<WGM50));
  TCCR5B &= ~((1<<WGM53)|(1<<WGM52)); 
  TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
  TCCR5B &= ~(1<<CS52);
  tcnt5 = TOP + 1 - timeMs / 1000.0 * (CPUFREQUENCY / PRESCALER);
  TCNT5 = tcnt5;
  return;
}

void timer5Enable()
{
	TIMSK5 |= (1<<TOIE5);     // enable timer
	TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
    TCCR5B &= ~(1<<CS52);
	return;
}

void timer5Disable()
{
	TIMSK5 &= ~(1<<TOIE5);
	TCCR5B &= ~((1<<CS52)|(1<<CS51)|(1<<CS50));
	return;
}


void stopWheels()
{
	leftMotor.stop();
	rightMotor.stop();
	return;
}
