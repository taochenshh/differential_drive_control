#include "myVNH5019motor.h"
myVNH5019Motor leftMotor(1);
myVNH5019Motor rightMotor(2);

#define TOP 0xFFFF
#define CPUFREQUENCY 16000000
#define PRESCALER 64
#define INTTIME 5
#define SPEEDLIMIT 30.0
#define SIZE 700

#define LEFTOFFSET 120
#define RIGHTOFFSET 120

void timer5Init(float);
void timer5Enable();
void timer5Disable();

float leftarray[SIZE];
float rightarray[SIZE];
int i;
int tcnt5;
int flag = 0;

float kp1 = 50;
float ki1 = 100;
float kd1 = 0;
float settingSpeed = SPEEDLIMIT ;
int outputPWMLeft;
int outputPWMRight;
float ek1Left = 0;
float ek2Left = 0;
float ek1Right = 0;
float ek2Right = 0;
float uk1Left = 0;
float uk1Right = 0;
float leftSpeed;
float rightSpeed;

void setup()
{
  Serial.begin(115200);
  delay(2000);
  Serial.println("Communication Successful");
  leftMotor.initmotor();
  rightMotor.initmotor();
  leftMotor.setDirection(0);
  rightMotor.setDirection(0);
  timer5Init(INTTIME);
  sei(); 
}

void loop()
{
  leftSpeed = leftMotor.getSpeed();
  rightSpeed = rightMotor.getSpeed();
  if(flag)
  {
    for (i = 0; i < SIZE; i++)
    {
      Serial.print(leftarray[i]);
      Serial.print("    ");
      Serial.println(rightarray[i]);
    }

    delay(50000);
  }
}

ISR(TIMER5_OVF_vect)
{
	float ukLeft;
	float ukRight;
	float deltaULeft;
	float deltaURight;
	float kpLeft;
	float kiLeft;
	float kdLeft;
	float kpRight;
	float kiRight;
	float kdRight;

	float ekLeft;
	float ekRight;

    TCNT5 = tcnt5; 	
	kpLeft = kp1;
	kiLeft = ki1;
	kdLeft = kd1;
	kpRight = kp1;
	kiRight = ki1;
	kdRight = kd1;
	
	if (outputPWMLeft >= 255 || outputPWMLeft <= 0)
	{
		kiLeft = 0;
	}
	if (outputPWMRight >= 255 || outputPWMRight <= 0)
	{
		kiRight = 0;
	}	
	leftarray[i] = leftSpeed;		
	rightarray[i] = rightSpeed;
	
	if (settingSpeed >= SPEEDLIMIT)
	{
		settingSpeed = SPEEDLIMIT;		
	}
	ekLeft = settingSpeed - leftSpeed;
	ekRight = settingSpeed - rightSpeed;
	
	deltaULeft += kpLeft * (ekLeft - ek1Left);
    deltaULeft += kiLeft * (INTTIME / 1000.0) * ekLeft;
	deltaULeft += kdLeft / (INTTIME / 1000.0) * (ekLeft - 2 * ek1Left + ek2Left);
	
    deltaURight += kpRight * (ekRight - ek1Right);
    deltaURight += kiRight * (INTTIME / 1000.0) * ekRight;
	deltaURight += kdRight / (INTTIME / 1000.0) * (ekRight - 2 * ek1Right + ek2Right);
	
	ukLeft = uk1Left + deltaULeft;
	ukRight = uk1Right + deltaURight;
	
	outputPWMLeft = ukLeft + LEFTOFFSET;
	outputPWMRight = ukRight + RIGHTOFFSET;
	
	if (outputPWMLeft >= 255)
	{
		outputPWMLeft = 255;
	}
	else if(outputPWMLeft <= 0)
	{
		outputPWMLeft = 0;
	}
	
    if (outputPWMRight >= 255)
	{
		outputPWMRight = 255;
	}
	else if(outputPWMRight <= 0)
	{
		outputPWMRight = 0;
	}
	
	ek1Left = ekLeft;
	ek2Left = ek1Left;
	
	ek1Right = ekRight;
	ek2Right = ek1Right;
	
	uk1Left = ukLeft;
	uk1Right = ukRight;
	leftMotor.outputPWM(outputPWMLeft);
	rightMotor.outputPWM(outputPWMRight);	
	
    i++;

    if (i == SIZE)
    {
        timer5Disable();
        flag = 1;
        leftMotor.setSpeed(0);
        rightMotor.setSpeed(0);
    }

}
void timer5Init(float timeMs)    // ms
{
  timer5Enable();
  TCCR5A &= ~((1<<WGM51)|(1<<WGM50));
  TCCR5B &= ~((1<<WGM53)|(1<<WGM52)); 
  TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
  TCCR5B &= ~(1<<CS52);
  tcnt5 = TOP + 1 - timeMs / 1000.0 * (CPUFREQUENCY / PRESCALER);
  TCNT5 = tcnt5;
  return;
}

void timer5Enable()
{
	TIMSK5 |= (1<<TOIE5);     // enable timer
  TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
  TCCR5B &= ~(1<<CS52);
	return;
}

void timer5Disable()
{
	TIMSK5 &= ~(1<<TOIE5);
  TCCR5B &= ~((1<<CS52)|(1<<CS51)|(1<<CS50));
	return;
}
