#include <Wire.h>
#include "myVNH5019motor.h"
#include <Filters.h>


#define TOP 0xFFFF
#define CPUFREQUENCY 16000000
#define PRESCALER 64
#define INTTIME 20
#define SPEEDLIMIT 60.0
#define HeadingAngle 180
#define SIZE 500

#define LEFTOFFSET 94.91
#define RIGHTOFFSET 126.09
#define SCALE 2

/* LSM303 Address definitions */
#define LSM303_MAG  0x1E  // assuming SA0 grounded
#define LSM303_ACC  0x18  // assuming SA0 grounded
 
#define X 0
#define Y 1
#define Z 2
 
/* LSM303 Register definitions */
#define CTRL_REG1_A 0x20
#define CTRL_REG2_A 0x21
#define CTRL_REG3_A 0x22
#define CTRL_REG4_A 0x23
#define CTRL_REG5_A 0x24
#define HP_FILTER_RESET_A 0x25
#define REFERENCE_A 0x26
#define STATUS_REG_A 0x27
#define OUT_X_L_A 0x28
#define OUT_X_H_A 0x29
#define OUT_Y_L_A 0x2A
#define OUT_Y_H_A 0x2B
#define OUT_Z_L_A 0x2C
#define OUT_Z_H_A 0x2D
#define INT1_CFG_A 0x30
#define INT1_SOURCE_A 0x31
#define INT1_THS_A 0x32
#define INT1_DURATION_A 0x33
#define CRA_REG_M 0x00
#define CRB_REG_M 0x01
#define MR_REG_M 0x02
#define OUT_X_H_M 0x03
#define OUT_X_L_M 0x04
#define OUT_Y_H_M 0x05
#define OUT_Y_L_M 0x06
#define OUT_Z_H_M 0x07
#define OUT_Z_L_M 0x08
#define SR_REG_M 0x09
#define IRA_REG_M 0x0A
#define IRB_REG_M 0x0B
#define IRC_REG_M 0x0C

//////////////////////////////////////////////////


myVNH5019Motor leftMotor(1);
myVNH5019Motor rightMotor(2);

float leftarray[SIZE];
float headingarray[SIZE];
int time[SIZE];

volatile unsigned int leftCount = 0;
volatile unsigned int rightCount = 0;
int i = 0;
int tcnt5;
int flag = 0;

float kp1 = 10;
float ki1 = 8;
float kd1 = 0.02;
float kp2 = 0.2;
float ki2 = 0;
float kd2 = 0;

int accel[3];  // we'll store the raw acceleration values here
int mag[3];  // raw magnetometer values stored here
float realAccel[3];  // calculated acceleration values here
float settingSpeed = SPEEDLIMIT ;
float settingHeading = HeadingAngle;
int outputPWMLeft;
int outputPWMRight;
float ek1Left = 0;
float ek2Left = 0;
float ek1Right = 0;
float ek2Right = 0;
float uk1Left = 0;
float uk1Right = 0;
int t;
float filterFrequency = 2.0;
float headingOffset = 0;
float readHeading;
FilterOnePole leftlowpassFilter(LOWPASS,filterFrequency);
FilterOnePole headinglowpassFilter(LOWPASS,filterFrequency);


void setup() 
{
	Serial.begin(115200);
	Wire.begin();
    Serial.println("Program Begins"); 
	initLSM303(SCALE);
	leftMotor.initmotor();
	rightMotor.initmotor();
	leftMotor.setDirection(0);
	rightMotor.setDirection(0);
    headingOffset = 180 - quickHeading();
	Serial.println("Angle Recorded, Please change facing direction");
	delay(2000);
   timer3Init();
	attachInterrupt(digitalPinToInterrupt(leftMotor.hallSensor), leftMInt, RISING);
	
  timer5Init(INTTIME);   //timer 5 interrupt every 10 ms	
    sei();   //enable global interrupt
    t = millis();
}

void loop() 
{

  readHeading = quickHeading();
	if(flag)
	{
		for (i = 0; i < SIZE; i++)
		{
			Serial.print(time[i]);
			Serial.print("    ");
			Serial.print(leftarray[i]);
			Serial.print("    ");
			Serial.println(headingarray[i]);
		}
		delay(50000);
	}
}

void timer3Init(void)
{
  /*no prescaling */
  TCCR3A=0x00;
  TCCR3B=(1<<CS30);
  TCNT3=0x00;
}
void leftMInt()
{
	leftCount++;
}



ISR(TIMER5_OVF_vect)
{
	float ukLeft;
	float ukRight;
	float deltaULeft;
	float deltaURight;
	float kpLeft;
	float kiLeft;
	float kdLeft;
	float kpRight;
	float kiRight;
	float kdRight;
float currentHeading;
float leftSpeed;
float rightOffset;

    float ekLeft;
    float ekRight;
	//Serial.println("AAA");
	kpLeft = kp1;
	kiLeft = ki1;
	kdLeft = kd1;
	kpRight = kp2;
	kiRight = ki2;
	kdRight = kd2;
	
	if (outputPWMLeft >= 255 || outputPWMLeft <= 0)
	{
		kiLeft = 0;
	}
	if (outputPWMRight >= 255 || outputPWMRight <= 0)
	{
		kiRight = 0;
	}
	
	leftSpeed = leftCount * 1000.0 / INTTIME / 19.0 * 60.0 / 16.0;
	leftlowpassFilter.input(leftSpeed);
	leftSpeed = leftlowpassFilter.output();
	leftarray[i] = leftSpeed;
        
	currentHeading = readHeading + headingOffset;
	if (currentHeading >= 360)
	{
		currentHeading -= 360;
	}
	else if (currentHeading < 0)
	{
		currentHeading -= 360;
	}
Serial.println(currentHeading);
	headinglowpassFilter.input(currentHeading);
	currentHeading = headinglowpassFilter.output();
	headingarray[i] = currentHeading;
	if (i == 0)
	{
		time[i] = millis() - t;
	}
	else
	{
		time[i] = millis() - time[i-1];
	}

	if (settingSpeed >= SPEEDLIMIT)
	{
		settingSpeed = SPEEDLIMIT;		
	}
	
	ekLeft = settingSpeed - leftSpeed;
	ekRight = settingHeading - currentHeading;
	
	deltaULeft += kpLeft * (ekLeft - ek1Left);
    deltaULeft += kiLeft * (INTTIME / 1000.0) * ekLeft;
	deltaULeft += kdLeft / (INTTIME / 1000.0) * (ekLeft - 2 * ek1Left + ek2Left);
	
    deltaURight += kpRight * (ekRight - ek1Right);
    deltaURight += kiRight * (INTTIME / 1000.0) * ekRight;
	deltaURight += kdRight / (INTTIME / 1000.0) * (ekRight - 2 * ek1Right + ek2Right);
	
	ukLeft = uk1Left + deltaULeft;
	ukRight = uk1Right + deltaURight;
	
	outputPWMLeft = ukLeft + LEFTOFFSET;
rightOffset = (leftSpeed + 212.45 ) / 1.6848;
	outputPWMRight = -ukRight + rightOffset;
	
	if (outputPWMLeft >= 255)
	{
		outputPWMLeft = 255;
	}
	else if(outputPWMLeft <= 0)
	{
		outputPWMLeft = 0;
	}
	
    if (outputPWMRight >= 255)
	{
		outputPWMRight = 255;
	}
	else if(outputPWMRight <= 0)
	{
		outputPWMRight = 0;
	}
	
	ek1Left = ekLeft;
	ek2Left = ek1Left;
	
	ek1Right = ekRight;
	ek2Right = ek1Right;
	
	uk1Left = ukLeft;
	uk1Right = ukRight;
	leftMotor.outputPWM(outputPWMLeft);
	rightMotor.outputPWM(outputPWMRight);	
    leftCount = 0;
    if (i >= SIZE)
    {
        timer5Disable();
        flag = 1;
        leftMotor.setSpeed(0);
        rightMotor.setSpeed(0);
    }
    else
    {
      i++;
    }	
}



void timer5Init(float timeMs)    // ms
{
	timer5Enable();
	TCCR5A &= ~((1<<WGM51)|(1<<WGM50));
	TCCR5B &= ~((1<<WGM53)|(1<<WGM52)); 
	TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
	TCCR5B &= ~(1<<CS52);
	tcnt5 = TOP + 1 - timeMs / 1000.0 * (CPUFREQUENCY / PRESCALER);
	TCNT5 = tcnt5;
	return;
}

void timer5Enable()
{
	TIMSK5 |= (1<<TOIE5);     // enable timer
	TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
    TCCR5B &= ~(1<<CS52);
	return;
}

void timer5Disable()
{
	TIMSK5 &= ~(1<<TOIE5);
	TCCR5B &= ~((1<<CS52)|(1<<CS51)|(1<<CS50));
	return;
}



void initLSM303(int fs)
{
  LSM303_write(0x27, CTRL_REG1_A);  // 0x27 = normal power mode, all accel axes on
  if ((fs==8)||(fs==4))
    LSM303_write((0x00 | (fs-fs/2-1)<<4), CTRL_REG4_A);  // set full-scale
  else
    LSM303_write(0x00, CTRL_REG4_A);
  LSM303_write(0x14, CRA_REG_M);  // 0x14 = mag 30Hz output rate
  LSM303_write(0x00, MR_REG_M);  // 0x00 = continouous conversion mode
}

float quickHeading()
{
   getLSM303_accel(accel);  // get the acceleration values and store them in the accel array
  while(!(LSM303_read(SR_REG_M) & 0x01))
    ;  // wait for the magnetometer readings to be ready
  getLSM303_mag(mag);  // get the magnetometer values, store them in mag
  //printValues(mag, accel);  // print the raw accel and mag values, good debugging
  
  for (int i=0; i<3; i++)
    realAccel[i] = accel[i] / pow(2, 15) * SCALE;  // calculate real acceleration values, in units of g
  
  /* print both the level, and tilt-compensated headings below to compare */
  //Serial.print(getHeading(mag), 3);  // this only works if the sensor is level
 // Serial.print("\t\t");  // print some tabs
 // Serial.println(getTiltHeading(mag, realAccel), 3);  // see how awesome tilt compensation is?!
  //delay(100);  // delay for serial readability 
  return(getTiltHeading(mag,realAccel));  
}

float getHeading(int * magValue)
{
  // see section 1.2 in app note AN3192
  float heading = 180*atan2(magValue[Y], magValue[X])/PI;  // assume pitch, roll are 0
  
  if (heading <0)
    heading += 360;
  
  return heading;
}

float getTiltHeading(int * magValue, float * accelValue)
{
  // see appendix A in app note AN3192 
  float pitch = asin(-accelValue[X]);
  float roll = asin(accelValue[Y]/cos(pitch));
  
  float xh = magValue[X] * cos(pitch) + magValue[Z] * sin(pitch);
  float yh = magValue[X] * sin(roll) * sin(pitch) + magValue[Y] * cos(roll) - magValue[Z] * sin(roll) * cos(pitch);
  float zh = -magValue[X] * cos(roll) * sin(pitch) + magValue[Y] * sin(roll) + magValue[Z] * cos(roll) * cos(pitch);

  float heading = 180 * atan2(yh, xh)/PI;
  if (yh >= 0)
    return heading;
  else
    return (360 + heading);
}

void getLSM303_mag(int * rawValues)
{
  Wire.beginTransmission(LSM303_MAG);
  Wire.write(OUT_X_H_M);
  Wire.endTransmission();
  Wire.requestFrom(LSM303_MAG, 6);
  for (int i=0; i<3; i++)
    rawValues[i] = (Wire.read() << 8) | Wire.read();
}

void getLSM303_accel(int * rawValues)
{
  rawValues[Z] = ((int)LSM303_read(OUT_X_L_A) << 8) | (LSM303_read(OUT_X_H_A));
  rawValues[X] = ((int)LSM303_read(OUT_Y_L_A) << 8) | (LSM303_read(OUT_Y_H_A));
  rawValues[Y] = ((int)LSM303_read(OUT_Z_L_A) << 8) | (LSM303_read(OUT_Z_H_A));  
  // had to swap those to right the data with the proper axis
}

byte LSM303_read(byte address)
{
  byte temp;
  
  if (address >= 0x20)
    Wire.beginTransmission(LSM303_ACC);
  else
    Wire.beginTransmission(LSM303_MAG);
    
  Wire.write(address);
  
  if (address >= 0x20)
    Wire.requestFrom(LSM303_ACC, 1);
  else
    Wire.requestFrom(LSM303_MAG, 1);
  while(!Wire.available())
    ;
  temp = Wire.read();
  Wire.endTransmission();
  
  return temp;
}

void LSM303_write(byte data, byte address)
{
  if (address >= 0x20)
    Wire.beginTransmission(LSM303_ACC);
  else
    Wire.beginTransmission(LSM303_MAG);
    
  Wire.write(address);
  Wire.write(data);
  Wire.endTransmission();
}
	
