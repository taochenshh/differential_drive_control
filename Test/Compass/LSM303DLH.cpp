#include "LSM303DLH.h"

LSM303DFROBOT::LSM303DFROBOT(int scale)
{
  Wire.begin();
	SCALE = scale;
}
void LSM303DFROBOT::initLSM303()
{
	LSM303_write(0x27, CTRL_REG1_A);  // 0x27 = normal power mode, all accel axes on
	if ((SCALE==8)||(SCALE==4))
	{
		LSM303_write((0x00 | (SCALE-SCALE/2-1)<<4), CTRL_REG4_A);  // set full-scale
	}
	else
	{
		LSM303_write(0x00, CTRL_REG4_A);
	}
	LSM303_write(0x14, CRA_REG_M);  // 0x14 = mag 30Hz output rate
	LSM303_write(0x00, MR_REG_M);  // 0x00 = continouous conversion mode
        return;
}

void LSM303DFROBOT::quickHeading()    // 1 to display the heading info
{
	getLSM303_accel();    // get the acceleration values and store them in the accelValue array
	while(!(LSM303_read(SR_REG_M)&0x01)); // wait for the magnetometer readings to be ready
	getLSM303_mag(); // get the magnetometer values, store them in magValue
	 
    for (int i=0; i<3; i++)
    {
		realAccel[i] = accelValue[i] / pow(2, 15) * SCALE;  // calculate real acceleration values, in units of g
	}
	nowHeading = getHeading();
	nowTiltHeading = getTiltHeading();
	//if (toDisplay == 1)
	//{
	//	Serial.print(nowHeading, 3);  // this only works if the sensor is level
	//	Serial.print("\t\t");  // print some tabs
	//	Serial.println(nowTiltHeading, 3);  // see how awesome tilt compensation is?!	
	//}
  return;
}
float LSM303DFROBOT::getTiltHeadingNum()
{
  return(nowTiltHeading);
}
float LSM303DFROBOT::getHeading()
{
	// see section 1.2 in app note AN3192
	float heading = 180*atan2(magValue[Y], magValue[X])/PI;  // assume pitch, roll are 0
	if (heading <0)
	{
		heading += 360;
	}

	return heading;	
}

float LSM303DFROBOT::getTiltHeading()
{
	// see appendix A in app note AN3192 
	float pitch = asin(-realAccel[X]);
	float roll = asin(realAccel[Y]/cos(pitch));
	float xh = magValue[X] * cos(pitch) + magValue[Z] * sin(pitch);
	float yh = magValue[X] * sin(roll) * sin(pitch) + magValue[Y] * cos(roll) - magValue[Z] * sin(roll) * cos(pitch);
	float zh = -magValue[X] * cos(roll) * sin(pitch) + magValue[Y] * sin(roll) + magValue[Z] * cos(roll) * cos(pitch);

	float heading = 180 * atan2(yh, xh)/PI;
 
	if (yh < 0)
	{
		heading += 360;
	}
	return (heading);

}

void LSM303DFROBOT::getLSM303_mag()
{
	Wire.beginTransmission(LSM303_MAG);
	Wire.write(OUT_X_H_M);
	Wire.endTransmission();
	Wire.requestFrom(LSM303_MAG, 6);
	for (int i=0; i<3; i++)
	{
		magValue[i] = (Wire.read() << 8) | Wire.read();
	}

}
 
void LSM303DFROBOT::getLSM303_accel()
{
	accelValue[Z] = ((int)LSM303_read(OUT_X_L_A) << 8) | (LSM303_read(OUT_X_H_A));
	accelValue[X] = ((int)LSM303_read(OUT_Y_L_A) << 8) | (LSM303_read(OUT_Y_H_A));
	accelValue[Y] = ((int)LSM303_read(OUT_Z_L_A) << 8) | (LSM303_read(OUT_Z_H_A));  
	// had to swap those to right the data with the proper axis
  return;
}
 
byte LSM303DFROBOT::LSM303_read(byte address)
{
	byte temp;

	if (address >= 0x20)
	{
		Wire.beginTransmission(LSM303_ACC);
	}
	else
	{
	    Wire.beginTransmission(LSM303_MAG);		
	}
	 
	Wire.write(address);

	if (address >= 0x20)
	{
		Wire.requestFrom(LSM303_ACC, 1);		
	}
	else
        {
	       Wire.requestFrom(LSM303_MAG, 1);
        }
	while(!Wire.available());
	temp = Wire.read();
	Wire.endTransmission();
	return temp;
}
 
void LSM303DFROBOT::LSM303_write(byte data, byte address)
{
  if (address >= 0x20)
    Wire.beginTransmission(LSM303_ACC);
  else
    Wire.beginTransmission(LSM303_MAG);
     
  Wire.write(address);
  Wire.write(data);
  Wire.endTransmission();
}
 

