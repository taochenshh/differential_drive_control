#include "myVNH5019motor.h"
#include "MC33926.h"
#include "myVNH5019motor.h"
#include <Filters.h>
#define LOWLIMIT 1000
#define HIGHLIMIT 2000
#define CH1 41 
#define CH2 42
#define CH3 43

#define INTTIME 5
#define TIME 20
#define SPEEDLIMIT 40.0
#define CPUFREQUENCY 16000000
#define PRESCALER 64
#define TOP 0xFFFF
#define LEFTOFFSET 102.67
#define RIGHTOFFSET 108.74

myVNH5019Motor leftMotor(1);
myVNH5019Motor rightMotor(2);
MC33926 rollerMotor;

int ch1;
int ch2;
int ch3;
int j = 0;

float settingSpeed = SPEEDLIMIT ;
int outputPWMLeft;
int outputPWMRight;
float ek1Left = 0;
float ek2Left = 0;
float ek1Right = 0;
float ek2Right = 0;
float uk1Left = 0;
float uk1Right = 0;
double errSumL = 0;
double errSumR = 0;
float kp1 = 30;
float ki1 = 2;
float kd1 = 0.1;
float kp2 = 30;
float ki2 = 4;
float kd2 = 0.07;
unsigned long t;
int tcnt5;
float filterFrequency = 4.0;


FilterOnePole leftlowpassFilter(LOWPASS,filterFrequency);
FilterOnePole rightlowpassFilter(LOWPASS,filterFrequency);

void setup() 
{
	Serial.begin(115200);
	Serial.println("Program begins");
	radioControlInit();
	leftMotor.initmotor();
	rightMotor.initmotor();
	rollerMotor.initMotor();

	//timer5Init(INTTIME);   

	//sei();   //enable global interrupt
	t = millis();
	Serial.println("Coming");
}

void loop() 
{  
    radioControl();
   	float ukLeft;
	float ukRight;
	float deltaULeft;
	float deltaURight;
	float kpLeft;
	float kiLeft;
	float kdLeft;
	float kpRight;
	float kiRight;
	float kdRight;
	float leftSpeed;
	float rightSpeed;
	float ekLeft;
	float ekRight;
	Serial.println(millis() - t);
	t=millis();
	
	kpLeft = kp1;
	kiLeft = ki1;
	kdLeft = kd1;
	kpRight = kp2;
	kiRight = ki2;
	kdRight = kd2;

	if (outputPWMLeft >= 255 || outputPWMLeft <= 0)
	{
		kiLeft = 0;
	}
	if (outputPWMRight >= 255 || outputPWMRight <= 0)
	{
		kiRight = 0;
	}	
	leftSpeed = leftMotor.getSpeed();
    leftlowpassFilter.input(leftSpeed);
    leftSpeed  = leftlowpassFilter.output();	
    rightSpeed = rightMotor.getSpeed();
    rightlowpassFilter.input(rightSpeed);
    rightSpeed = rightlowpassFilter.output();
	

	if (settingSpeed >= SPEEDLIMIT)
	{
		settingSpeed = SPEEDLIMIT;		
	}
	ekLeft = settingSpeed - leftSpeed;
	ekRight = settingSpeed - rightSpeed;
	
	
	deltaULeft += kpLeft * (ekLeft - ek1Left);
    deltaULeft += kiLeft * (TIME / 1000.0) * ekLeft;
	deltaULeft += kdLeft / (TIME / 1000.0) * (ekLeft - 2 * ek1Left + ek2Left);
	
    deltaURight += kpRight * (ekRight - ek1Right);
    deltaURight += kiRight * (TIME / 1000.0) * ekRight;
	deltaURight += kdRight / (TIME / 1000.0) * (ekRight - 2 * ek1Right + ek2Right);
	
	ukLeft = uk1Left + deltaULeft;
	ukRight = uk1Right + deltaURight;

	outputPWMLeft = ukLeft + LEFTOFFSET;
	outputPWMRight = ukRight + RIGHTOFFSET;
	
	if (outputPWMLeft >= 255)
	{
		outputPWMLeft = 255;
	}
	else if(outputPWMLeft <= 0)
	{
		outputPWMLeft = 0;
	}
	
        if (outputPWMRight >= 255)
	{
		outputPWMRight = 255;
	}
	else if(outputPWMRight <= 0)
	{
		outputPWMRight = 0;
	}
	
	ek1Left = ekLeft;
	ek2Left = ek1Left;
	
	ek1Right = ekRight;
	ek2Right = ek1Right;
	
	uk1Left = ukLeft;
	uk1Right = ukRight;
	leftMotor.outputPWM(outputPWMLeft);
	rightMotor.outputPWM(outputPWMRight);	

}
void radioControl()
{
	int ch1;
	int ch2;
	int ch3;
	int absch3;

          //timer5Disable();

	ch1=pulseIn(CH1,HIGH,15000);
	ch1=map(ch1,1160,1800,255,-255);
	ch1=constrain(ch1, -255, 255);
	if(ch1 < 25 && ch1 > -25)
	{
		ch1=0;
	}
	
	if (ch1 == 0)
	{
            ch2=pulseIn(CH2,HIGH,15000);
	    ch2=map(ch2,1300,1900,-255,255);
	    ch2=constrain(ch2, -255, 255);		
		if(ch2 < 30 && ch2 > -30)
		{
			ch2=0;
		}		
	}
		
	
	ch3=pulseIn(CH3,HIGH,15000);
	ch3=map(ch3,1080,1900,-255,255 );
	ch3=constrain(ch3, -255,255);
    if(ch3 < 90 && ch3 > -90)
	{
		ch3=0;
	}
	
	Serial.print(ch1);
	Serial.print("   ");
	Serial.print(ch2);
	Serial.print("   ");
	Serial.println(ch3);
	absch3 = abs(ch3);
	



    if(ch1 == 0 && ch2 == 0)
	{
		settingSpeed = 0;
	    stopWheels();
	}	
    else if(ch1 != 0)
	{
		settingSpeed = map(abs(ch1),0,255,0,SPEEDLIMIT);
		if(ch1 > 0)
		{
			leftMotor.setDirection(0);
			rightMotor.setDirection(0);
		}
		else
		{
			leftMotor.setDirection(1);
			rightMotor.setDirection(1);
		}
		//leftMotor.setSpeed(ch1);
		//rightMotor.setSpeed(ch1);
	}
	else if(ch2 != 0)
	{
		settingSpeed = map(abs(ch2),0,255,0,SPEEDLIMIT);
		if(ch2 > 0)
		{
			leftMotor.setDirection(0);
			rightMotor.setDirection(1);			
		}
		else
		{
		    leftMotor.setDirection(1);
			rightMotor.setDirection(0);	
		}
		//leftMotor.setSpeed(-ch2);
		//rightMotor.setSpeed(ch2);
	}	
	
	if(ch3 == 0)
	{
		rollerMotor.stop();
	}
	else if (ch3 > 0)
	{
		rollerMotor.setDirection(1);
		rollerMotor.setSpeed(absch3);	
		rollerMotor.start();		
	}
	else if (ch3 < 0)
	{
		rollerMotor.setDirection(0);
		rollerMotor.setSpeed(absch3);
		rollerMotor.start();
	} 		
	return;	
}
/*
ISR(TIMER5_OVF_vect,ISR_NOBLOCK)
{
	timer5Disable();

}*/
/*
// Changing Time Interval
ISR(TIMER5_OVF_vect,ISR_NOBLOCK)
{
	timer5Disable();
	float ukLeft;
	float ukRight;
	float deltaULeft;
	float deltaURight;
	float kpLeft;
	float kiLeft;
	float kdLeft;
	float kpRight;
	float kiRight;
	float kdRight;
	float leftSpeed;
	float rightSpeed;
	unsigned long tk;
    double time;
	float ekLeft;
	float ekRight;

    TCNT5 = tcnt5; 	
	kpLeft = kp1;
	kiLeft = ki1;
	kdLeft = kd1;
	kpRight = kp2;
	kiRight = ki2;
	kdRight = kd2;

	if (outputPWMLeft >= 255 || outputPWMLeft <= 0)
	{
		kiLeft = 0;
	}
	if (outputPWMRight >= 255 || outputPWMRight <= 0)
	{
		kiRight = 0;
	}	
	leftSpeed = leftMotor.getSpeed();
    leftlowpassFilter.input(leftSpeed);
    leftSpeed  = leftlowpassFilter.output();		
	rightSpeed = rightMotor.getSpeed();
    rightlowpassFilter.input(rightSpeed);
    rightSpeed = rightlowpassFilter.output();
	
	tk = millis();
	time = (double)(tk - tk1);
	tk1 = tk;
	if (settingSpeed >= SPEEDLIMIT)
	{
		settingSpeed = SPEEDLIMIT;		
	}
	ekLeft = settingSpeed - leftSpeed;
	ekRight = settingSpeed - rightSpeed;
	

	UkLeft = kpLeft * (ekLeft - ek1Left);
	errSumL += kiLeft * (time / 1000.0) * ekLeft;
	UkLeft += errSumL;
	UkLeft += kdLeft / (time / 1000.0) * (ekLeft - ek1Left);
	
    UkRight = kpRight * (ekRight - ek1Right);
	errSumR += kiRight * (time / 1000.0) * ekRight;
	UkRight += errSumR;
	UkRight += kdRight / (time / 1000.0) * (ekRight - ek1Right);
	outputPWMLeft = ukLeft + LEFTOFFSET;
	outputPWMRight = ukRight + RIGHTOFFSET;
	
	if (outputPWMLeft >= 255)
	{
		outputPWMLeft = 255;
	}
	else if(outputPWMLeft <= 0)
	{
		outputPWMLeft = 0;
	}
	
        if (outputPWMRight >= 255)
	{
		outputPWMRight = 255;
	}
	else if(outputPWMRight <= 0)
	{
		outputPWMRight = 0;
	}
	
	ek1Left = ekLeft;	
	ek1Right = ekRight;

	leftMotor.outputPWM(outputPWMLeft);
	rightMotor.outputPWM(outputPWMRight);	
	
	timer5Enable();
} 
*/
void radioControlInit()  
{
  pinMode(CH1,INPUT);
  pinMode(CH2,INPUT);
  pinMode(CH3,INPUT);
  return;
}

void timer5Init(float timeMs)    // ms
{
  timer5Enable();
  TCCR5A &= ~((1<<WGM51)|(1<<WGM50));
  TCCR5B &= ~((1<<WGM53)|(1<<WGM52)); 
  TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
  TCCR5B &= ~(1<<CS52);
  tcnt5 = TOP + 1 - timeMs / 1000.0 * (CPUFREQUENCY / PRESCALER);
  TCNT5 = tcnt5;
  return;
}

void timer5Enable()
{
	TIMSK5 |= (1<<TOIE5);     // enable timer
	TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
    TCCR5B &= ~(1<<CS52);
	return;
}

void timer5Disable()
{
	TIMSK5 &= ~(1<<TOIE5);
	TCCR5B &= ~((1<<CS52)|(1<<CS51)|(1<<CS50));
	return;
}


void stopWheels()
{
	leftMotor.stop();
	rightMotor.stop();
	return;
}
