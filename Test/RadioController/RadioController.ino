#include "myVNH5019motor.h"

#define LOWLIMIT 1000
#define HIGHLIMIT 2000
#define CH1 3
#define CH2 20
#define CH3 21
#define CH5 44
#define INTTIME 10
#define SPEEDLIMIT 160.0
#define CPUFREQUENCY 16000000
#define PRESCALER 64
#define TOP 0xFFFF

int ch1;
int ch2;
int ch3;
int ch5;

myVNH5019Motor leftMotor(1);
myVNH5019Motor rightMotor(2);
void setup() 
{
  Serial.begin(115200);
  radioControlInit();
  	leftMotor.initmotor();
	rightMotor.initmotor();
stopWheels();
}

void loop() 
{
	ch5=pulseIn(CH5,HIGH,25000);
	ch5=map(ch5,LOWLIMIT,HIGHLIMIT,-255,255);
	ch5=constrain(ch5, -255, 255);
//Serial.print(leftMotor.getSpeed());
Serial.print("  ");
//Serial.println(rightMotor.getSpeed());

//Serial.print("ch5: ");
//Serial.println(ch5);
radioControl();
//delay(1000);
//delay(1000);

  // put your main code here, to run repeatedly:
}
void radioControl()
{
	int ch1;
	int ch2;
	int ch3;
	int absch3;

	ch1=pulseIn(CH1,HIGH,25000);
	ch2=pulseIn(CH2,HIGH,25000);
	ch3=pulseIn(CH3,HIGH,25000);

	ch1=map(ch1,LOWLIMIT,HIGHLIMIT,-255,255);
	ch1=constrain(ch1, -255, 255);
	ch2=map(ch2,LOWLIMIT,HIGHLIMIT,-255,255);
	ch2=constrain(ch2, -255, 255);
	ch3=map(ch3,LOWLIMIT,HIGHLIMIT,-255,255 );
	ch3=constrain(ch3, -255,255);
	Serial.print(ch1);
Serial.print("   ");
	Serial.print(ch2);
Serial.print("   ");
	Serial.println(ch3);
  /*  absch3 = abs(ch3);
	if(ch1 < 20 && ch1 > -20)
	{
		ch1=0;
	}
	if(ch2 < 50 && ch2 > -50)
	{
		ch2=0;
	}
	if(ch3 < 120 && ch3 > -120)
	{
		ch3=0;
	}

    if(ch1 == 0 && ch2 == 0)
	{
	    stopWheels();
	}	
    else if(ch1 != 0)
	{
		leftMotor.setSpeed(ch1);
		rightMotor.setSpeed(ch1);
	}
	else if(ch2 != 0)
	{
		leftMotor.setSpeed(-ch2);
		rightMotor.setSpeed(ch2);
	}

	*/
	return;	
}

void radioControlInit()  
{
  pinMode(CH1,INPUT);
  pinMode(CH2,INPUT);
  pinMode(CH3,INPUT);
  pinMode(CH5,INPUT);
  return;
}

void stopWheels()
{
	leftMotor.stop();
	rightMotor.stop();
	return;
}
