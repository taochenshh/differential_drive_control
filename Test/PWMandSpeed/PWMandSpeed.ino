#include "myVNH5019motor.h"
myVNH5019Motor leftMotor(1);
myVNH5019Motor rightMotor(2);

void setup() 
{
  Serial.begin(115200);
  delay(2000);
  Serial.println("Communication Begins");
  leftMotor.initmotor();
  rightMotor.initmotor();
  leftMotor.setDirection(0);
  rightMotor.setDirection(0);

}

void loop() {
  int i = 0;
  for (i = 0; i <= 255; i += 5)
  {
    leftMotor.outputPWM(i);
    rightMotor.outputPWM(i);
    delay(2000);
    Serial.print(i);
    Serial.print("    ");
    Serial.print(leftMotor.getSpeed());
    Serial.print("    ");
    Serial.println(rightMotor.getSpeed());
  }
}
