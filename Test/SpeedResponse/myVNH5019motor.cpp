#include "myVNH5019motor.h"
myVNH5019Motor::myVNH5019Motor(unsigned char choice)
{
    INA = 2;            // choice != 2 , motor 1
	INB = 4;
	ENDIAG = 6;
	CS = A0;
    PWMPIN = 9;	
	hallSensor = 18;  
  if (choice == 2)      // choice == 2, motor 2
  {
	INA = 7;
	INB = 8;
	ENDIAG = 12;
	CS = A1;  	
    PWMPIN = 10;	
	hallSensor = 19;  
  }	
}
void myVNH5019Motor::initmotor()
{
	pinMode(INA, OUTPUT);
	pinMode(INB, OUTPUT);
	pinMode(PWMPIN, OUTPUT);
	pinMode(ENDIAG,OUTPUT);
	pinMode(CS,INPUT);
	digitalWrite(ENDIAG,HIGH);
	stop();
	
	// Arduino Mega 2560
	// set pwm frequency 
	// fast pwm
	// clear OC2 on compare Match, set OC2 at Bottom
	// TOP = 0xFF
	// prescale = 8;
	// PWM frequency = 16 MHz / 8 prescaler / 255 (top) = 7.84 KHz
	TCCR2A = 0b10100011;
	TCCR2B = 0b00000010;
    return;
}

void myVNH5019Motor::forward()
{
	setSpeed(255);
    return; 
}

void myVNH5019Motor::reverse()
{
	setSpeed(-255);	
    return;
}

void myVNH5019Motor::setDirection(bool direction)
{
	// 1 for + ,  0 for -
	if (direction == 1)
	{
		digitalWrite(INA, LOW);
		digitalWrite(INB, HIGH);
	}
	else if(direction == 0)
	{
		digitalWrite(INA, HIGH);
		digitalWrite(INB, LOW);
	}
    return;
}
void myVNH5019Motor::setSpeed(int speed)
{
	// speed value should be between -255 and 255
	int absSpeed;
	
	if (speed > 0)
	{
		setDirection(0);
		absSpeed = -speed;
	}
	else
	{
		setDirection(1);
		absSpeed  = speed;
	}
	
	if (absSpeed >= 255)
	{
		absSpeed = 255;
	}
	analogWrite(PWMPIN,absSpeed);
	return;
}
void myVNH5019Motor::outputPWM(int pwm)
{
	analogWrite(PWMPIN,pwm);	
}

void myVNH5019Motor::stop()
{
	digitalWrite(INA,HIGH);
	digitalWrite(INB,HIGH);
	return;
}

unsigned int myVNH5019Motor::getCurrent()
{
	// 5V / 1024 ADC counts / 144 mV per A = 34 mA per count
	return analogRead(CS) * 34;   // unit: mA
}

float myVNH5019Motor::getSpeed()
{
	unsigned long highLength;
	unsigned long lowLength;
	float speed;
	
	highLength = pulseIn(hallSensor,HIGH,20000);
	//lowLength = pulseIn(hallSensor,LOW,100000);
	//speed = 1.0 / ((highLength + lowLength) / 1000000.0) / 16.0;  // rpm
    speed = 1.0 / ((highLength  * 2) / 1000000.0) / 16.0;	
    if (speed >= 400)
	{
		speed = 0.0;
	}
	return(speed);
}



