#include "myVNH5019motor.h"
#include <Filters.h>
#include <Arduino.h>

myVNH5019Motor leftMotor(1);
myVNH5019Motor rightMotor(2);
#define LEFTOFFSET 94.91
#define RIGHTOFFSET 126.09
#define TOP 0xFFFF
#define CPUFREQUENCY 16000000
#define PRESCALER 64
#define INTTIME 20
#define SIZE 300
#define SPEEDLIMIT 80.0
void timer5Init(float);
void timer5Enable();
void timer5Disable();

int tcnt5;
volatile unsigned int leftCount = 0;
volatile unsigned int rightCount = 0;

float filterFrequency = 2.0;

FilterOnePole leftlowpassFilter(LOWPASS,filterFrequency);
FilterOnePole rightlowpassFilter(LOWPASS,filterFrequency);
volatile float leftSpeed;
volatile float rightSpeed;
volatile float filtedLeftSpeed;
volatile float filtedRightSpeed;
float kp1 = 10;  
float ki1 = 8; 
float kd1 = 0.02;
float kp2 = 12;   
float ki2 = 8;   
float kd2 = 0.02;
float leftSpeedArray[SIZE];
float rightSpeedArray[SIZE];
int i = 0;
bool flag = 0;
volatile float settingSpeed = SPEEDLIMIT ;
int outputPWMLeft;
int outputPWMRight;
float ek1Left = 0;
float ek2Left = 0;
float ek1Right = 0;
float ek2Right = 0;
float uk1Left = 0;
float uk1Right = 0;
void setup()
{
	Serial.begin(115200);
	delay(2000);
	//Serial.println("Communication Successful");
	leftMotor.initmotor();
	rightMotor.initmotor();
	leftMotor.setDirection(0);
	rightMotor.setDirection(0);
       attachInterrupt(digitalPinToInterrupt(leftMotor.hallSensor), leftMInt, RISING);
	attachInterrupt(digitalPinToInterrupt(rightMotor.hallSensor), rightMInt, RISING);
	timer5Init(INTTIME);
	sei(); 
}

void loop()
{
	if (flag == 1)
	{
		for(i = 0; i < SIZE; i++)
		{
			Serial.print(leftSpeedArray[i]);
			Serial.print("   ");
			Serial.println(rightSpeedArray[i]);			
		}
        delay(50000);		
	}
 

}

void leftMInt()
{
	leftCount++;
}

void rightMInt()
{
	rightCount++;
}

ISR(TIMER5_OVF_vect)
{

	float ukLeft;
	float ukRight;
	float deltaULeft;
	float deltaURight;
	float kpLeft;
	float kiLeft;
	float kdLeft;
	float kpRight;
	float kiRight;
	float kdRight;
	float leftSpeed;
	float rightSpeed;
	float ekLeft;
	float ekRight;

    TCNT5 = tcnt5; 	
	kpLeft = kp1;
	kiLeft = ki1;
	kdLeft = kd1;
	kpRight = kp2;
	kiRight = ki2;
	kdRight = kd2;

	if (outputPWMLeft >= 255 || outputPWMLeft <= 0)
	{
		kiLeft = 0;
	}
	if (outputPWMRight >= 255 || outputPWMRight <= 0)
	{
		kiRight = 0;
	}	
	leftSpeed = leftCount * 1000.0 / INTTIME / 19.0 * 60.0 / 16.0;
  leftlowpassFilter.input(leftSpeed);
   leftSpeed  = leftlowpassFilter.output();		
	rightSpeed = rightCount * 1000.0 / INTTIME / 19.0 * 60.0 / 16.0;
  rightlowpassFilter.input(rightSpeed);
 rightSpeed = rightlowpassFilter.output();
	
	if (i < SIZE)
	{
		leftSpeedArray[i] = leftSpeed;
		rightSpeedArray[i] = rightSpeed;
 i++;
	if (settingSpeed >= SPEEDLIMIT)
	{
		settingSpeed = SPEEDLIMIT;		
	}
	ekLeft = settingSpeed - leftSpeed;
	ekRight = settingSpeed - rightSpeed;
	
	
	deltaULeft += kpLeft * (ekLeft - ek1Left);
    deltaULeft += kiLeft * ( INTTIME / 1000.0) * ekLeft;
	deltaULeft += kdLeft / (INTTIME / 1000.0) * (ekLeft - 2 * ek1Left + ek2Left);
	
    deltaURight += kpRight * (ekRight - ek1Right);
    deltaURight += kiRight * (INTTIME / 1000.0) * ekRight;
	deltaURight += kdRight / (INTTIME / 1000.0) * (ekRight - 2 * ek1Right + ek2Right);
	
	ukLeft = uk1Left + deltaULeft;
	ukRight = uk1Right + deltaURight;

	outputPWMLeft = ukLeft + LEFTOFFSET;
	outputPWMRight = ukRight + RIGHTOFFSET;
	
	if (outputPWMLeft >= 255)
	{
		outputPWMLeft = 255;
	}
	else if(outputPWMLeft <= 0)
	{
		outputPWMLeft = 0;
	}
	
        if (outputPWMRight >= 255)
	{
		outputPWMRight = 255;
	}
	else if(outputPWMRight <= 0)
	{
		outputPWMRight = 0;
	}
	
	ek1Left = ekLeft;
	ek2Left = ek1Left;
	
	ek1Right = ekRight;
	ek2Right = ek1Right;
	
	uk1Left = ukLeft;
	uk1Right = ukRight;
	leftMotor.outputPWM(outputPWMLeft);
	rightMotor.outputPWM(outputPWMRight);	
	}
else
{
  flag = 1;
  settingSpeed = 0;
  leftMotor.outputPWM(0);
  rightMotor.outputPWM(0);
}
       

	leftCount = 0;
	rightCount = 0;
}

void timer5Init(float timeMs)    // ms
{
	TCCR5A &= ~((1<<WGM51)|(1<<WGM50));
	TCCR5B &= ~((1<<WGM53)|(1<<WGM52)); 
	TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
	TCCR5B &= ~(1<<CS52);
	tcnt5 = TOP + 1 - timeMs / 1000.0 * (CPUFREQUENCY / PRESCALER);
	TCNT5 = tcnt5;
	timer5Enable();
	return;
}

void timer5Enable()
{
	TIMSK5 |= (1<<TOIE5);     // enable timer
	TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
	TCCR5B &= ~(1<<CS52);
	return;
}

void timer5Disable()
{
	TIMSK5 &= ~(1<<TOIE5);
	TCCR5B &= ~((1<<CS52)|(1<<CS51)|(1<<CS50));
	return;
}

