#include <Wire.h>
#include <Filters.h>
#include "myVNH5019motor.h"


#define TOP 0xFFFF
#define CPUFREQUENCY 16000000
#define PRESCALER 64
#define INTTIME 20
#define HeadingAngle 180

#define LEFTOFFSET 94.91
#define RIGHTOFFSET 126.09
#define SCALE 2

/* LSM303 Address definitions */
#define LSM303_MAG  0x1E  // assuming SA0 grounded
#define LSM303_ACC  0x18  // assuming SA0 grounded
 
#define X 0
#define Y 1
#define Z 2
 
/* LSM303 Register definitions */
#define CTRL_REG1_A 0x20
#define CTRL_REG2_A 0x21
#define CTRL_REG3_A 0x22
#define CTRL_REG4_A 0x23
#define CTRL_REG5_A 0x24
#define HP_FILTER_RESET_A 0x25
#define REFERENCE_A 0x26
#define STATUS_REG_A 0x27
#define OUT_X_L_A 0x28
#define OUT_X_H_A 0x29
#define OUT_Y_L_A 0x2A
#define OUT_Y_H_A 0x2B
#define OUT_Z_L_A 0x2C
#define OUT_Z_H_A 0x2D
#define INT1_CFG_A 0x30
#define INT1_SOURCE_A 0x31
#define INT1_THS_A 0x32
#define INT1_DURATION_A 0x33
#define CRA_REG_M 0x00
#define CRB_REG_M 0x01
#define MR_REG_M 0x02
#define OUT_X_H_M 0x03
#define OUT_X_L_M 0x04
#define OUT_Y_H_M 0x05
#define OUT_Y_L_M 0x06
#define OUT_Z_H_M 0x07
#define OUT_Z_L_M 0x08
#define SR_REG_M 0x09
#define IRA_REG_M 0x0A
#define IRB_REG_M 0x0B
#define IRC_REG_M 0x0C

//////////////////////////////////////////////////


myVNH5019Motor leftMotor(1);
myVNH5019Motor rightMotor(2);
volatile int i = 0;
int tcnt5;
float settingHeading = HeadingAngle;
volatile unsigned long t;
int accel[3];  // we'll store the raw acceleration values here
int mag[3];  // raw magnetometer values stored here
float realAccel[3];  // calculated acceleration values here
float filterFrequency = 2.0;
float headingOffset = 0;
FilterOnePole headinglowpassFilter(LOWPASS,filterFrequency);

float currentHeading;
void setup() 
{
	Serial.begin(115200);
	Wire.begin();
    Serial.println("Program Begins"); 
	initLSM303(SCALE);
    headingOffset = 180 - quickHeading();
	//timer5Init(INTTIME);   //timer 5 interrupt every 10 ms	
    //sei();   //enable global interrupt
  //  t = millis();
}

void loop() 
{
currentHeading = quickHeading() + headingOffset;

		if (currentHeading >= 360)
		{
			currentHeading -= 360;
		}
		else if (currentHeading < 0)
		{
			currentHeading -= 360;
		}
		Serial.print("NF:");
		Serial.print(currentHeading);
		headinglowpassFilter.input(currentHeading);
		currentHeading = headinglowpassFilter.output();
		Serial.print("  F:");
		Serial.println(currentHeading);
}


//ISR(TIMER5_OVF_vect,ISR_NOBLOCK)
ISR(TIMER5_OVF_vect)
{
	timer5Disable();
sei();
	unsigned long time;
	float currentHeading;
		time = millis();
		Serial.println(time - t);
		t = time;
		currentHeading = quickHeading() + headingOffset;

		if (currentHeading >= 360)
		{
			currentHeading -= 360;
		}
		else if (currentHeading < 0)
		{
			currentHeading -= 360;
		}
		//Serial.print("NF:");
		//Serial.print(currentHeading);
		headinglowpassFilter.input(currentHeading);
		currentHeading = headinglowpassFilter.output();
		//Serial.print("  F:");
		//Serial.println(currentHeading);	
     //   i = 0;		
	//}
    timer5Enable();
    Serial.println(millis() - time);
}





void timer5Init(float timeMs)    // ms
{
	timer5Enable();
	TCCR5A &= ~((1<<WGM51)|(1<<WGM50));
	TCCR5B &= ~((1<<WGM53)|(1<<WGM52)); 
	TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
	TCCR5B &= ~(1<<CS52);
	tcnt5 = TOP + 1 - timeMs / 1000.0 * (CPUFREQUENCY / PRESCALER);
	TCNT5 = tcnt5;
	return;
}

void timer5Enable()
{
	TIMSK5 |= (1<<TOIE5);     // enable timer
	TCCR5B |= (1<<CS51)|(1<<CS50);  // 64 prescaler
    TCCR5B &= ~(1<<CS52);
	return;
}

void timer5Disable()
{
	TIMSK5 &= ~(1<<TOIE5);
	TCCR5B &= ~((1<<CS52)|(1<<CS51)|(1<<CS50));
	return;
}



void initLSM303(int fs)
{
  LSM303_write(0x27, CTRL_REG1_A);  // 0x27 = normal power mode, all accel axes on
  if ((fs==8)||(fs==4))
    LSM303_write((0x00 | (fs-fs/2-1)<<4), CTRL_REG4_A);  // set full-scale
  else
    LSM303_write(0x00, CTRL_REG4_A);
  LSM303_write(0x14, CRA_REG_M);  // 0x14 = mag 30Hz output rate
  LSM303_write(0x00, MR_REG_M);  // 0x00 = continouous conversion mode
}

float quickHeading()
{
   getLSM303_accel(accel);  // get the acceleration values and store them in the accel array
  while(!(LSM303_read(SR_REG_M) & 0x01))
    ;  // wait for the magnetometer readings to be ready
  getLSM303_mag(mag);  // get the magnetometer values, store them in mag
  //printValues(mag, accel);  // print the raw accel and mag values, good debugging
  
  for (int i=0; i<3; i++)
    realAccel[i] = accel[i] / pow(2, 15) * SCALE;  // calculate real acceleration values, in units of g
  
  /* print both the level, and tilt-compensated headings below to compare */
  //Serial.print(getHeading(mag), 3);  // this only works if the sensor is level
 // Serial.print("\t\t");  // print some tabs
 // Serial.println(getTiltHeading(mag, realAccel), 3);  // see how awesome tilt compensation is?!
  //delay(100);  // delay for serial readability 
  return(getTiltHeading(mag,realAccel));  
}

float getHeading(int * magValue)
{
  // see section 1.2 in app note AN3192
  float heading = 180*atan2(magValue[Y], magValue[X])/PI;  // assume pitch, roll are 0
  
  if (heading <0)
    heading += 360;
  
  return heading;
}

float getTiltHeading(int * magValue, float * accelValue)
{
  // see appendix A in app note AN3192 
  float pitch = asin(-accelValue[X]);
  float roll = asin(accelValue[Y]/cos(pitch));
  
  float xh = magValue[X] * cos(pitch) + magValue[Z] * sin(pitch);
  float yh = magValue[X] * sin(roll) * sin(pitch) + magValue[Y] * cos(roll) - magValue[Z] * sin(roll) * cos(pitch);
  float zh = -magValue[X] * cos(roll) * sin(pitch) + magValue[Y] * sin(roll) + magValue[Z] * cos(roll) * cos(pitch);

  float heading = 180 * atan2(yh, xh)/PI;
  if (yh >= 0)
    return heading;
  else
    return (360 + heading);
}

void getLSM303_mag(int * rawValues)
{
  Wire.beginTransmission(LSM303_MAG);
  Wire.write(OUT_X_H_M);
  Wire.endTransmission();
  Wire.requestFrom(LSM303_MAG, 6);
  for (int i=0; i<3; i++)
    rawValues[i] = (Wire.read() << 8) | Wire.read();
}

void getLSM303_accel(int * rawValues)
{
  rawValues[Z] = ((int)LSM303_read(OUT_X_L_A) << 8) | (LSM303_read(OUT_X_H_A));
  rawValues[X] = ((int)LSM303_read(OUT_Y_L_A) << 8) | (LSM303_read(OUT_Y_H_A));
  rawValues[Y] = ((int)LSM303_read(OUT_Z_L_A) << 8) | (LSM303_read(OUT_Z_H_A));  
  // had to swap those to right the data with the proper axis
}

byte LSM303_read(byte address)
{
  byte temp;
  
  if (address >= 0x20)
    Wire.beginTransmission(LSM303_ACC);
  else
    Wire.beginTransmission(LSM303_MAG);
    
  Wire.write(address);
  
  if (address >= 0x20)
    Wire.requestFrom(LSM303_ACC, 1);
  else
    Wire.requestFrom(LSM303_MAG, 1);
  while(!Wire.available())
    ;
  temp = Wire.read();
  Wire.endTransmission();
  
  return temp;
}

void LSM303_write(byte data, byte address)
{
  if (address >= 0x20)
    Wire.beginTransmission(LSM303_ACC);
  else
    Wire.beginTransmission(LSM303_MAG);
    
  Wire.write(address);
  Wire.write(data);
  Wire.endTransmission();
}
	
